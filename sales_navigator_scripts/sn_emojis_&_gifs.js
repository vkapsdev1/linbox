function callXHROnLinkedInAttachments(filename, filesize, filetype, a, c, b, d){

    if(filetype.indexOf("image") >= 0){
        var pdata = {
                  mediaUploadType: 'MESSAGING_PHOTO_ATTACHMENT',
                  fileSize: filesize,
                  filename: filename
                };
    }else{
        var pdata = {
                  mediaUploadType: 'MESSAGING_FILE_ATTACHMENT',
                  fileSize: filesize,
                  filename: filename
                };
    }
    
    $.ajax({
        url: a,
        data: JSON.stringify(pdata),
        async: !1,
        headers: { "Accept":"application/vnd.linkedin.normalized+json+2.1", "Content-Type":"application/json; charset=UTF-8" },
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "POST",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}
function callXHROnLinkedInAttachments_second_call(file, a, c, b, d){

                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function(){
                    if(xhr.readyState == 4 && xhr.status == 201){
                        "function" == typeof b && b(a)
                    } else {
                        "function" == typeof b && b()
                            
                    }
                };
                xhr.open("PUT",a, false);
                xhr.send(file);

}

function callXHROnLinkedInConversationAttachmentCreate(pdata, a, c, b, d) {

    $.ajax({
        url: a,
        data: JSON.stringify(pdata),
        async: !1,
        headers: { "Accept":"application/vnd.linkedin.normalized+json+2.1", "Content-Type":"application/json; charset=UTF-8" },
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "POST",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}

$(document).ready(function(){

    if( window.location.origin == "https://mail.google.com") {

        $('body').on('click', '.sn_attach_icon', function(){
            $(this).next('.sn_attachment_icon').click();  
        });

        $('body').on('change', '.sn_attachment_icon', function(e){

            e.target.baseURI = "https://www.linkedin.com/";

            var ocean_native_file1 = $(this).val();

            console.log($(this).val());

            console.log(e);

            var fileName = e.target.files[0].name;
            var fileSize = e.target.files[0].size;
            var fileType = e.target.files[0].type;
            var file = e.target.files[0];
            var blob_url = URL.createObjectURL(file);

            var byteSize = fileSize;
            if(fileSize > 1024 ){

                byteSize = Math.round(fileSize/1024) + "KB";
                if(byteSize > 1024){
                    byteSize = Math.round(byteSize/1024) + "MB";
                }
            }else{
                byteSize = fileSize + "b";
            }


            var conversation_id = $('.sn_conversation_section').data('conversation_id');

            var storage_key = e.target.files[0].size+"_attachment_urn";
            if(fileType == "image/png"){
                $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><img src='"+blob_url+"' /><span class='sn_attachment_name'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                
                setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }    
            if(fileType == "image/jpeg"){
                $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><img src='"+blob_url+"' /><span class='sn_attachment_name'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }                

            if(fileType == "application/vnd.ms-excel"){
                $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><span class='sn_attachment_name xls_attachment'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }

            if(fileType == "application/pdf"){
                $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><span class='sn_attachment_name pdf_attachment'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }
            
            if(fileType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
                 $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><span class='sn_attachment_name doc_attachment'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                 setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }

            if(fileType == "application/octet-stream" || fileType == "application/zip" || fileType.indexOf("video") >= 0){
                  $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><img class='file_attachment' src='"+chrome.runtime.getURL("images/file-icon.png")+"' /><span class='sn_attachment_name'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                  setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }

            if(fileType == "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
                $('.sn_attachment_box').append("<div class='sn_attachment' data-storage_key='"+storage_key+"'><span class='sn_attachment_name ppt_attachment'>"+fileName+"</span><span class='sn_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='sn_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.sn_attachment_box .sn_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.sn_attachment_box .sn_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }
            
            chrome.runtime.sendMessage({greeting: "sn_attachment_request_1", attachment_filename: fileName, attachment_filesize: fileSize, attachment_filetype: fileType, attachment_fileblob: blob_url, linkedin_conversation_id: conversation_id}, function(response) {});
        });

        $('body').on('click', '.sn_attachment_remove_btn', function(){
            var key = $(this).closest('.sn_attachment').data('storage_key');;
            chrome.storage.local.remove([key]);
            $(this).closest('.sn_attachment').remove();
        });

    }
    var pathname = window.location.pathname;
    if( window.location.origin == "https://www.linkedin.com" && pathname.indexOf('/sales/') >= 0) {
        

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

            if( request.ocean == "sn_attachment_response_1" ){
                console.log(request);

                window.sn_blob_url = '';
                var x = new XMLHttpRequest();
                x.onload = function() {
                    var result = x.response;

                    var binaryData = [];
                    binaryData.push(result);
                    
                    var file = new Blob(binaryData, {type: request.attachment_filetype});

                    var new_u = URL.createObjectURL(new Blob(binaryData, {type: request.attachment_filetype}));
                    
                    var url = "https://www.linkedin.com/sales-api/salesApiMediaUploadMetadata?action=upload";
                    console.log(request);
                    callXHROnLinkedInAttachments(request.attachment_filename, request.attachment_filesize, request.attachment_filetype, url, [{
                        key: "x-restli-protocol-version",
                        val: "2.0.0"
                    }], function(a) {
                        var url = a.data.value.singleUploadUrl;
                        var attachment_id = a.data.value.urn;

                        var xhr = new XMLHttpRequest();
                        xhr.onreadystatechange = function(){
                            if(xhr.readyState == 4 && xhr.status == 201){
                                console.log("second call");

                                var guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

                                var pdata = {
                                              eventCreate: {
                                                originToken: guid,
                                                value: {
                                                  'com.linkedin.voyager.messaging.create.MessageCreate': {
                                                    attributedBody: {
                                                      text: "",
                                                      attributes: []
                                                    },
                                                    attachments: [{"id":attachment_id,"name":request.attachment_filename,"byteSize":request.attachment_filesize,"mediaType":request.attachment_filetype,"reference":{"string":new_u}}],
                                                    extensionContent: {}
                                                  }
                                                }
                                              },
                                              dedupeByClientGeneratedToken: false
                                            };

                                chrome.storage.local.set({[request.attachment_filesize+'_attachment_urn']: JSON.stringify({"mediaId":attachment_id, "attachmentUrn": attachment_id, "name":request.attachment_filename,"size":request.attachment_filesize,"mimeType":request.attachment_filetype})}, function() {
                                });

                            } else {
                                console.log(xhr.status);
                            }
                        };
                        xhr.open("PUT",url, false);
                        xhr.send(file);

                    });
                     // TODO: Use [object ArrayBuffer]
                };
                x.open('GET', request.attachment_fileblob); // <-- blob:-url created in content script
                x.responseType = 'arraybuffer';
                x.send();
            }

        });
        
    }


    

});
