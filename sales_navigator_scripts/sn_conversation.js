function sn_getProfileDetailsForRegistration (identifier, csrf_token = null, b) {
    console.log("ocean");
    if (identifier == "undefined") {
        "function" == typeof b && b(false);
    } else {

        var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
        $.ajax({
            url: 'https://www.linkedin.com/voyager/api/identity/profiles/'+identifier+'/profileView',
            beforeSend: function(req) {
                if(csrf_token == null){
                    var csrf_token_o = document.cookie.match(JSESSIONID_REGEX)[1];    
                }else{
                    console.log(csrf_token);
                    var csrf_token_o = csrf_token;
                }  
                req.setRequestHeader('csrf-token', csrf_token_o);
            },
            xhrFields: {
                withCredentials: true
            },
            success: function(response) {

                var obj = {};
                if (response && response.profile) {
                    var profile = response.profile;
                    var entityUrn = response.entityUrn;
                    entityUrn = entityUrn.replace('urn:li:fs_profileView:', '');
                    obj = {
                        firstName: profile.firstName,
                        lastName: profile.lastName,
                        industry: profile.industryName,
                        location: profile.locationName
                    }
                    if (profile && profile.picture && profile.picture['com.linkedin.voyager.common.MediaProcessorImage'] && profile.picture['com.linkedin.voyager.common.MediaProcessorImage'].id) {
                        obj.profile_img = 'https://media.licdn.com/mpr/mpr/shrinknp_100_100' + profile.picture['com.linkedin.voyager.common.MediaProcessorImage'].id;
                    } else if (profile && profile.miniProfile && profile.miniProfile.picture && profile.miniProfile.picture['com.linkedin.common.VectorImage']) {
                        var vectorImg = profile.miniProfile.picture['com.linkedin.common.VectorImage'];
                        if (vectorImg.artifacts && vectorImg.artifacts.length > 0) {
                            obj.profile_img = vectorImg['rootUrl'] + '' + vectorImg.artifacts.splice(-1)[0].fileIdentifyingUrlPathSegment;
                        }
                    }
                    obj.linkedin_profile_id = entityUrn;
                    obj.linkedin_profile_url = 'https://www.linkedin.com/in/' + obj.linkedin_profile_id;
                    $.ajax({
                        url: 'https://www.linkedin.com/voyager/api/identity/profiles/' + identifier + '/profileContactInfo',
                        beforeSend: function(req) {
                            var csrf_token = document.cookie.match(JSESSIONID_REGEX)[1];
                            req.setRequestHeader('csrf-token', csrf_token);
                        },
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function(contact_info) {
                            if (contact_info && contact_info.emailAddress) {
                                obj.email = contact_info.emailAddress;
                                if (obj.email.indexOf('phishing') >= 0) {
                                    obj.email = decodeURIComponent(obj.email).replace(/(.*?)https:.*?=(.*)/, '$1$2');
                                }
                            }
                            /* 
                             * Put your API call here to signup to Felix with obj
                             * We can get more data, please check the response of both API calls and add more to obj
                             *
                             */
                            "function" == typeof b && b(obj)
                        }
                    });
                }
            }
        })
    }
}

function callXHROnLinkedInConversation(a, c, b, d) {
    $.ajax({
        url: a,
        async: !1,
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "GET",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}


function callXHROnLinkedInConversationReadSingle(id = '', a, c, b, d) {
    if (id == '') {
        var time = new Date().getTime();
        var pdata = {
            patch: {
                $set: {
                    read: true
                }
            }
        };
    } else {
        var conversation = 'urn:li:fs_conversation:' + id;
        var pdata = {
            items: [
                conversation
            ]
        };
    }

    $.ajax({
        url: a,
        data: JSON.stringify(pdata),
        headers: {
            "Accept": "application/vnd.linkedin.normalized+json+2.1",
            "Content-Type": "application/json; charset=UTF-8"
        },
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "POST",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}

function callXHROnLinkedInConversationCreate(img_media, text, CompoundKey, itemId, a, c, b, d) {

    var guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    if (img_media != '') {

        var pdata = {
            eventCreate: {
                originToken: guid,
                value: {
                    'com.linkedin.voyager.messaging.create.MessageCreate': {
                        attributedBody: {
                            text: "",
                            attributes: []
                        },
                        attachments: [],
                        extensionContent: {
                            extensionContentType: "THIRD_PARTY_MEDIA",
                            thirdPartyMedia: img_media
                        }
                    }
                }
            },
            dedupeByClientGeneratedToken: false
        };
        $.ajax({
            url: a,
            data: JSON.stringify(pdata),
            async: !1,
            headers: {
                "Accept": "application/vnd.linkedin.normalized+json+2.1",
                "Content-Type": "application/json; charset=UTF-8"
            },
            beforeSend: function(a) {
                var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
                var b = document.cookie.match(JSESSIONID_REGEX)[1];
                a.setRequestHeader("csrf-token", b);
                c && 0 < c.length && c.forEach(function(b) {
                    a.setRequestHeader(b.key, b.val)
                })
            },
            xhrFields: {
                withCredentials: !0
            },
            type: "POST",
            success: function(a) {
                "function" == typeof b && b(a)
            },
            error: function(a) {
                "function" == typeof b && b()
            }
        });

    } else {
        var attachment_json = new Array();

        function getValue(callback) {
            chrome.storage.local.get(null, callback);
        }


        getValue(function(value) {
            var allKeys = Object.keys(value);
            var allValues = Object.values(value);
            $.each(allKeys, function(i, e) {

                if (e.indexOf('attachment_urn') >= 0) {
                    attachment_json[i] = JSON.parse(value[e]);
                    chrome.storage.local.remove([e]);
                }

            });

            var pdata = {
                sendMessageInput:
                    {
                        "body": text,
                        "inmail":false,
                        "replyItemId": itemId,
                        "attachments":attachment_json,
                        "recipients":[{
                            "recepientCompoundKey": CompoundKey
                        }]
                    }
            };
            $.ajax({
                url: a,
                data: JSON.stringify(pdata),
                async: !1,
                headers: {
                    "Accept": "application/vnd.linkedin.normalized+json+2.1",
                    "Content-Type": "application/json; charset=UTF-8"
                },
                beforeSend: function(a) {
                    var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
                    var b = document.cookie.match(JSESSIONID_REGEX)[1];
                    a.setRequestHeader("csrf-token", b);
                    c && 0 < c.length && c.forEach(function(b) {
                        a.setRequestHeader(b.key, b.val)
                    })
                },
                xhrFields: {
                    withCredentials: !0
                },
                type: "POST",
                success: function(a) {
                    "function" == typeof b && b(a)
                },
                error: function(a) {
                    "function" == typeof b && b()
                }
            });
        });

    }

}

function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}

function getAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function get_sn_conversation_results(data) {

    sn_conversations_detail = new Array();

    data = data.reverse();

    $.each(data, function(i, e) {


        sn_conversations_detail[i] = new Array();

        var participant_key = '';
        $.each(e.toGroup, function(t, g){
            participant_key = g;
        });

        // Fetching profile name 
        sn_conversations_detail[i]["name"] = e.fromResolutionResult.fullName;

        // Fetching profile picture
        if(typeof e.fromResolutionResult.profilePictureDisplayImage !== "undefined"){
            sn_conversations_detail[i]["profile_pic"] = e.fromResolutionResult.profilePictureDisplayImage.artifacts[0].fileIdentifyingUrlPathSegment;
        }else{
            sn_conversations_detail[i]["profile_pic"] = '';
        }    

        var identifier = e.from;
        identifier = identifier.replace("urn:li:fs_salesProfile:(", "");
        identifier = identifier.replace(")", "");
        sn_conversations_detail[i]["itemId"] = identifier;
        identifier = identifier.split(',');

        sn_conversations_detail[i]["publicIdentifier"] = identifier[0];

        sn_conversations_detail[i]["body"] = e.body;

        sn_conversations_detail[i]["createdAt"] = e.createdDate;

        sn_conversations_detail[i]["subtype"] = e.itemStatus;

        if(typeof e.attachments !== "undefined"){
            var attachments = e.attachments;
            if(attachments.length > 0){
                
                var attachment = new Array();

                $.each(attachments, function(x, y){

                    attachment[x] = new Array();

                    attachment[x]["url"] = y.reference;  
                    attachment[x]["name"] = y.name;  
                    attachment[x]["mediaType"] = y.mimeType;  
                    var byteSize = y.size;
                    if(y.size > 1024 ){

                        byteSize = Math.round(y.size/1024) + "KB";
                        if(byteSize > 1024){
                            byteSize = Math.round(byteSize/1024) + "MB";
                        }
                    }else{
                        byteSize = y.size + "b";
                    }
                    attachment[x]["size"] = byteSize;  

                });

                sn_conversations_detail[i]["attachment"] = attachment;
            }
        }

    });

    return sn_conversations_detail;

}

$(document).ready(function() {


    if (window.location.origin == "https://mail.google.com") {


        $('body').on('click', '.sn_threads_table .sn_thread_row', function() {
            $('.sales_navigator_box_wrapper').remove();
            window.sn_initilize = false;
            var conversation_id = $(this).data('conversation_id');
            var conversation_title = $(this).data('title');
            var conversation_image = $(this).data('profile_pic');
            var conversation_occupation = $(this).data('occupation');
            var conversation_publicIdentifier = $(this).data('publicidentifier');
            var conversation_itemID = $(this).data('itemid');

            var conversation_seenAt = $(this).data('seenat');

            $('.linkedin_mailbox_container_right .linkedin_loader_wrap').show();

            chrome.storage.local.get(null, function(items){
                var allKeys   = Object.keys(items);
                $.each(allKeys, function(i, e){
                    if(e.indexOf('attachment_urn') >= 0 ){
                        chrome.storage.local.remove([e]);
                    }
                });
            });
            console.log(conversation_id);
            chrome.runtime.sendMessage({
                greeting: "sn_conversation_request_1",
                sn_conversation_id: conversation_id,
                sn_conversation_title: conversation_title,
                sn_conversation_image: conversation_image,
                sn_conversation_occupation: conversation_occupation,
                sn_conversation_seenAt: conversation_seenAt,
                sn_conversation_publicIdentifier: conversation_publicIdentifier,  sn_conversation_itemId: conversation_itemID
            }, function(response) {});

            window.sn_conversation_clicked = $(this);

        });

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

            if (request.ocean == "sn_conversation_response_2") {

                window.repeat = true;

                console.log(request);

                var sn_conversations_detail = get_sn_conversation_results(request.result);
                console.log("ocean idhar");
                console.log(sn_conversations_detail);
                var sn_conversations_html = "<div class='sn_conversation_section' data-conversation_id='" + request.sn_conversation_id + "' data-conversation_seenAt='" + request.sn_conversation_seenAt + "' data-conversation_image='" + request.sn_conversation_image + "' data-publicIdentifier='" + request.sn_conversation_publicIdentifier + "' data-itemid='" + request.sn_conversation_itemId + "' data-conversation_title='" + request.sn_conversation_title + "' data-conversation_occupation='" + request.sn_conversation_occupation + "'>";

                window.sn_seenAt = true;

                window.sn_sametime = '';
                window.sn_sameperson = '';

                $.each(
                    sn_conversations_detail,
                    function(g, h) {

                        if (h.name == request.owner_linkedin_profile_id) {
                            var participent_class = 'owner_profile';
                            var participent_profile_link = "https://www.linkedin.com/in/me";

                        } else {
                            var participent_class = 'other_profile';
                            var participent_profile_link = "https://www.linkedin.com/in/" + request.sn_conversation_publicIdentifier;
                        }

                        var message_combine_flag = true;

                        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                        var dateObj = new Date(h.createdAt);
                        var time = getAMPM(dateObj);
                        var month = dateObj.getUTCMonth() + 1; //months from 1-12
                        var day = dateObj.getUTCDate();
                        var year = dateObj.getUTCFullYear();

                        var c_dateObj = new Date();
                        var c_month = c_dateObj.getUTCMonth() + 1; //months from 1-12
                        var c_day = c_dateObj.getUTCDate();
                        var c_year = c_dateObj.getUTCFullYear();

                        if (window.sn_sametime == '') {

                            window.sn_sametime = day + " " + months[month - 1] + " " + time;
                            window.sn_sameperson = h.name;
                            var same_con_time = false;
                            var same_con_person = false;
                        } else {

                            if (window.sn_sametime == day + " " + months[month - 1] + " " + time && window.sn_sameperson == h.name) {
                                var same_con_time = true;

                            } else {
                                window.sn_sameperson = h.name;
                                window.sn_sametime = day + " " + months[month - 1] + " " + time;
                                var same_con_time = false;
                            }
                        }

                        if (c_day + "/" + c_month + "/" + c_year == day + "/" + month + "/" + year) {
                            var createdAt = time;
                        } else {
                            var createdAt = +day + " " + months[month - 1] + " " + time;
                        }

                        //if (h.subtype !== "INVITATION_ACCEPT") {

                            if (typeof h.profile_pic !== "undefined" && h.profile_pic != "") {
                                profile_pic = h.profile_pic;
                            } else {
                                profile_pic = chrome.runtime.getURL("images/no-profile.png");
                            }


                            if (same_con_time == false) {

                                message_combine_flag = false;


                                if (g > 0) {

                                    sn_conversations_html += "</div></div>";

                                }

                                sn_conversations_html += "<div class='linkedin_conversation_container " + participent_class + "' data-timestamp='" + h.createdAt + "'><div class='linkedin_conversation_first'><div class='linkedin_conversation_profile_pic'><img src='" + profile_pic + "' /></div><div class='linkedin_conversation_name'><a href='" + participent_profile_link + "'>" + h.name + "</a></div><div class='linkedin_conversation_datetime'>" + createdAt + "</div></div><div class='linkedin_conversation_second'>";

                            }

                            sn_conversations_html += "<div class='linkedin_conversation_body'>";

                            if (validURL(h.body)) {

                                sn_conversations_html += "<img src='" + h.body + "' />";

                            } else {

                                sn_conversations_html += "<pre>" + h.body + "</pre>";
                                if (typeof h.attachment !== "undefined") {
                                    console.log(h.attachment);
                                    attachment = h.attachment;
                                    if (attachment.length > 0) {

                                        $.each(attachment, function(s, r) {

                                            if (r.mediaType == "image/jpeg" || r.mediaType == "image/png") {
                                                sn_conversations_html += "<a class='linkedin_conversation_lightbox' href='" + r.url + "'><img src='" + r.url + "' /> </a>";
                                            }

                                            else if (r.mediaType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                                                sn_conversations_html += "<a class='sn_attachment_download xls_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            else if (r.mediaType == "application/pdf") {
                                                sn_conversations_html += "<a class='sn_attachment_download pdf_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            else if (r.mediaType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                                                sn_conversations_html += "<a class='sn_attachment_download doc_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            

                                            else if (r.mediaType == "application/powerpoint") {
                                                sn_conversations_html += "<a class='sn_attachment_download ppt_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            else {
                                                sn_conversations_html += "<a class='sn_attachment_download other_attachment' href='" + r.url + "'><img src='" + chrome.runtime.getURL("images/file-icon.png") + "' /><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                        });

                                    }
                                }
                            }

                            if (window.sn_seenAt && request.sn_conversation_seenAt != "") {

                                if (h.createdAt >= request.sn_conversation_seenAt) {

                                    sn_conversations_html += "<div class='linkedin_conversation_seen'><img src='" + request.sn_conversation_image + "' /></div>";
                                    window.sn_seenAt = false;
                                }
                            }


                            sn_conversations_html += "</div>";




                        //}
                    });

                sn_conversations_html += "</div></div>";

                sn_conversations_html += "</div><div class='linkedin-msg-type-box'><div class='sn_attachment_box'></div><div class='inner-type-box'><textarea class='linkedin-msg-textarea' placeholder='Type here...'></textarea><ul class='icon-list'><li><div class='icons sn_attach_icon'></div><input class='sn_attachment_icon' type='file' style='display:none'/></li></ul><button class='sn_send-btn'>Send</button></div></div>";

                $('body').find('.linkedin_mailbox_container_right .linkedin_mailbox_container').html(sn_conversations_html);

                $('.linkedin_mailbox_container_right .linkedin_loader_wrap').hide();

                $('body').find('.sn_conversation_section').scrollTop($('.sn_conversation_section')[0].scrollHeight);

            }

            if (request.ocean == "sn_conversation_scroll_response_2") {

                var
                    sn_conversations_detail = get_sn_conversation_results(request.result);
                var sn_conversations_html = "";

                window.sn_sametime = '';
                window.sn_sameperson = '';



                $.each(
                    sn_conversations_detail,
                    function(g, h) {
                        console.log(h.publicIdentifier + '==' + request.owner_linkedin_profile_id);

                        console.log(window.linkedin_cookie);

                        if (h.name == request.owner_linkedin_profile_id) {
                            var participent_class = 'owner_profile';
                            var participent_profile_link = "https://www.linkedin.com/in/me";

                        } else {
                            var participent_class = 'other_profile';
                            var participent_profile_link = "https://www.linkedin.com/in/" + request.linkedin_conversation_publicIdentifier;
                        }

                        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                        var dateObj = new Date(h.createdAt);
                        var time = getAMPM(dateObj);
                        var month = dateObj.getUTCMonth() + 1; //months from 1-12
                        var day = dateObj.getUTCDate();
                        var year = dateObj.getUTCFullYear();

                        var c_dateObj = new Date();
                        var c_month = c_dateObj.getUTCMonth() + 1; //months from 1-12
                        var c_day = c_dateObj.getUTCDate();
                        var c_year = c_dateObj.getUTCFullYear();

                        if (window.sn_sametime == '') {

                            window.sn_sametime = day + " " + months[month - 1] + " " + time;
                            window.sn_sameperson = h.name;
                            var same_con_time = false;
                            var same_con_person = false;
                        } else {

                            if (window.sn_sametime == day + " " + months[month - 1] + " " + time && window.sn_sameperson == h.name) {
                                var same_con_time = true;

                            } else {
                                window.sn_sameperson = h.name;
                                window.sn_sametime = day + " " + months[month - 1] + " " + time;
                                var same_con_time = false;
                            }
                        }

                        if (c_day + "/" + c_month + "/" + c_year == day + "/" + month + "/" + year) {
                            var createdAt = time;
                        } else {
                            var createdAt = +day + " " + months[month - 1];
                        }

                        // if (h.subtype !== "INVITATION_ACCEPT") {

                            if (typeof h.profile_pic !== "undefined" && h.profile_pic != "") {
                                profile_pic = h.profile_pic;
                            } else {
                                profile_pic = chrome.runtime.getURL("images/no-profile.png");
                            }


                            if (same_con_time == false) {

                                message_combine_flag = false;


                                if (g > 0) {

                                    sn_conversations_html += "</div></div>";

                                }

                                sn_conversations_html += "<div class='linkedin_conversation_container " + participent_class + "' data-timestamp='" + h.createdAt + "'><div class='linkedin_conversation_first'><div class='linkedin_conversation_profile_pic'><img src='" + profile_pic + "' /></div><div class='linkedin_conversation_name'><a href='" + participent_profile_link + "'>" + h.name + "</a></div><div class='linkedin_conversation_datetime'>" + createdAt + "</div></div><div class='linkedin_conversation_second'>";

                            }

                            sn_conversations_html += "<div class='linkedin_conversation_body'>";

                            if (validURL(h.body)) {

                                sn_conversations_html += "<img src='" + h.body + "' />";

                            } else {

                                sn_conversations_html += "<pre>" + h.body + "</pre>";
                                if (typeof h.attachment !== "undefined") {
                                    attachment = h.attachment;
                                    if (attachment.length > 0) {

                                        $.each(attachment, function(s, r) {

                                            if (r.mediaType == "image/jpeg" || r.mediaType == "image/png") {
                                                sn_conversations_html += "<a class='linkedin_conversation_lightbox' href='" + r.url + "'><img src='" + r.url + "' /> </a>";
                                            }

                                            else if (r.mediaType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                                                sn_conversations_html += "<a class='sn_attachment_download xls_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            else if (r.mediaType == "application/pdf") {
                                                sn_conversations_html += "<a class='sn_attachment_download pdf_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            else if (r.mediaType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                                                sn_conversations_html += "<a class='sn_attachment_download doc_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            

                                            else if (r.mediaType == "application/powerpoint") {
                                                sn_conversations_html += "<a class='sn_attachment_download ppt_attachment' href='" + r.url + "'><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                            else {
                                                sn_conversations_html += "<a class='sn_attachment_download other_attachment' href='" + r.url + "'><img src='" + chrome.runtime.getURL("images/file-icon.png") + "' /><span class='sn_attachment_name'>" + r.name + "</span><span class='sn_attachment_size'>" + r.size + "</span></a>";
                                            }

                                        });

                                    }
                                }
                            }

                            sn_conversations_html += "</div>";

                        // }
                    });

                sn_conversations_html += "</div></div>";

                $('body').find('.sn_conversation_section').prepend(sn_conversations_html);

                $('.linkedin_mailbox_container_right .linkedin_loader_wrap').hide();

                window.repeat = true;
            }

            if (request.ocean == "sn_conversation_sendmsg_response_2") {
                console.log('sn_conversation_sendmsg_response_2');
                console.log(request);
                if (typeof request.result.threadUrn !== "undefined") {

                    var conversation_id = $('.sn_conversation_section').data('conversation_id');
                    var conversation_image = $('.sn_conversation_section').data('conversation_image');
                    var conversation_seenAt = $('.sn_conversation_section').data('conversation_seenat');
                    var conversation_title = $('.sn_conversation_section').data('conversation_title');
                    var conversation_publicIdentifier = $('.sn_conversation_section').data('publicidentifier');
                    var conversation_occupation = $('.sn_conversation_section').data('conversation_occupation');
                    var conversation_itemID = $('.sn_conversation_section').data('itemid');

                    chrome.runtime.sendMessage({
                        greeting: "sn_conversation_request_1",
                        sn_conversation_id: conversation_id,
                        sn_conversation_title: conversation_title,
                        sn_conversation_image: conversation_image,
                        sn_conversation_occupation: conversation_occupation,
                        sn_conversation_seenAt: conversation_seenAt,
                        sn_conversation_publicIdentifier: conversation_publicIdentifier,  sn_conversation_itemId: conversation_itemID
                    }, function(response) {

                        console.log('final call');
                    });

                    $('.linkedin-msg-textarea').val("");
                }
            }

            sn_listenForScrollEvent($(".sn_conversation_section"));
            $('body').on('sn-scroll', '.sn_conversation_section', function(e) {

                if ($('.sn_conversation_section').length > 0) {
                    
                    if ($(this).scrollTop() == 0) {
                        if (window.repeat) {

                            window.repeat = false;

                            var most_recent_conversation_timestamp = $('.sn_conversation_section .linkedin_conversation_container:first-child').data('timestamp');

                            var conversation_id = $('.sn_conversation_section').data('conversation_id');

                            $('.linkedin_mailbox_container_right .linkedin_loader_wrap').show();

                            chrome.runtime.sendMessage({
                                greeting: "sn_conversation_scroll_request_1",
                                conversation_timestamp: most_recent_conversation_timestamp,
                                sn_conversation_id: conversation_id
                            }, function(response) {});

                        }

                    }

                }
            });

        });


        $('body').on('click', '.sn_send-btn', function() {
            var msg_text = $('.linkedin-msg-textarea').val();
            var conversation_id = $('.sn_conversation_section').data('conversation_id');
            var itemId = $('.sn_conversation_section').data('itemid');
            chrome.runtime.sendMessage({
                greeting: "sn_conversation_sendmsg_request_1",
                sn_conversation_msg_text: msg_text,
                sn_conversation_id: conversation_id,
                sn_conversation_gif_obj: "", 
                sn_conversation_itemId: itemId
            }, function(response) {});
        });

    }
    var pathname = window.location.pathname;
    if( window.location.origin == "https://www.linkedin.com" && pathname.indexOf('/sales/') >= 0) {

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if (request.ocean == "sn_conversation_response_1") {

                callXHROnLinkedInConversation("https://www.linkedin.com/sales-api/salesApiMailbox?decoration=%28uniqueId%2Csubject%2Cbody%2CblockCopy%2CfooterText%2Cread%2CitemType%2CcustomPropertyType%2CsalesMessageType%2CinmailRestriction%2CitemStatus%2Creplied%2Cinbox%2Cpending%2Carchived%2CcreatedDate%2ClastModified%2CentityUrn%2Cattachments*%28name%2CmediaId%2CmimeType%2Csize%2Creference%2CattachmentUrn%29%2Cfrom~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%2CmemberBadges%29%2CtoGroup*~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%2CmemberBadges%2Cdegree%29%29&count=10&q=findThreadItems&threadId=" + request.sn_conversation_id, [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    
                    var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
                    var csrf_token = document.cookie.match(JSESSIONID_REGEX)[1];    

                    a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");
                    var owner_linkedin_profile_id = '';

                    sn_getProfileDetailsForRegistration ('me', null, function(res) {
                        // owner_linkedin_profile_id = res.linkedin_profile_id;
                        owner_linkedin_profile_id = res.firstName + ' ' +res.lastName;
                        chrome.runtime.sendMessage({
                            greeting: "sn_conversation_request_2",
                            "sn_conversation_id": request.sn_conversation_id,
                            "sn_conversation_title": request.sn_conversation_title,
                            "sn_conversation_image": request.sn_conversation_image,
                            "sn_conversation_occupation": request.sn_conversation_occupation,
                            sn_conversation_seenAt: request.sn_conversation_seenAt,
                            sn_conversation_publicIdentifier: request.sn_conversation_publicIdentifier, sn_conversation_itemId: request.sn_conversation_itemId,
                            owner_linkedin_profile_id: owner_linkedin_profile_id,
                            result: a.elements, csrf_token: csrf_token
                        }, function(response) {
                            console.log(response);
                        });
                    });
                });

                sn_getProfileDetailsForRegistration (request.sn_conversation_publicIdentifier, null, function(res) {
                    console.log('sn_sales_navigator_request_2 fail pass');
                    if(res == false){
                        console.log('idhar');
                        chrome.runtime.sendMessage({greeting: "sn_sales_navigator_request_2", result: 'no-profile' , gmail_inbox: 'false'}, function(response) {

                            });
                    }else{
                        console.log('udhar');
                        callXHROnLinkedInConversation("https://www.linkedin.com/sales/gmail/profile/viewByEmail/"+res.email, [{
                            key: "x-restli-protocol-version",
                            val: "2.0.0"
                        }], function(a) {
                            console.log(a);
                                chrome.runtime.sendMessage({greeting: "sn_sales_navigator_request_2", result:a , gmail_inbox: 'false' }, function(response) {

                            });

                        });    
                    }
                });

                // callXHROnLinkedInConversationReadSingle( request.linkedin_conversation_id, "https://www.linkedin.com/voyager/api/messaging/badge?action=markItemsAsSeen", [{
                //     key: "x-restli-protocol-version",
                //     val: "2.0.0"
                // }], function(a) { });

                // callXHROnLinkedInConversationReadSingle("","https://www.linkedin.com/voyager/api/messaging/conversations/"+request.linkedin_conversation_id, [{
                //     key: "x-restli-protocol-version",
                //     val: "2.0.0",
                //     referer: "https://www.linkedin.com/messaging/thread/"+request.linkedin_conversation_id+"/"
                // }], function(a) {  });

            }

            if (request.ocean == "sn_conversation_scroll_response_1") {

                callXHROnLinkedInConversation("https://www.linkedin.com/sales-api/salesApiMailbox?decoration=%28uniqueId%2Csubject%2Cbody%2CblockCopy%2CfooterText%2Cread%2CitemType%2CcustomPropertyType%2CsalesMessageType%2CinmailRestriction%2CitemStatus%2Creplied%2Cinbox%2Cpending%2Carchived%2CcreatedDate%2ClastModified%2CentityUrn%2Cattachments*%28name%2CmediaId%2CmimeType%2Csize%2Creference%2CattachmentUrn%29%2Cfrom~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%2CmemberBadges%29%2CtoGroup*~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%2CmemberBadges%2Cdegree%29%29&count=10&createdBefore=" + request.most_recent_conversation_timestamp + "&q=findThreadItems&threadId=" + request.sn_conversation_id, [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");

                    sn_getProfileDetailsForRegistration ('me', null, function(res) {
                        // var owner_linkedin_profile_id = res.sn_profile_id;
                        owner_linkedin_profile_id = res.firstName + ' ' + res.lastName;

                        chrome.runtime.sendMessage({
                            greeting: "sn_conversation_scroll_request_2",
                            result: a.elements,
                            owner_linkedin_profile_id: owner_linkedin_profile_id
                        }, function(response) {

                        });

                    });

                });

            }

            if(request.ocean == "sn_conversation_sendmsg_response_1"){
                console.log(request);
                // img_media, text, CompoundKey, itemId,
                callXHROnLinkedInConversationCreate( "", request.sn_conversation_msg_text, request.sn_conversation_itemId, request.sn_conversation_id, "https://www.linkedin.com/sales-api/salesApiMessaging?action=sendMessage", [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {

                    a && a.data.value && 0 < a.data.value.length ? console.log(a.data.value) : console.log("Request failed");
                    console.log(a);
                    chrome.runtime.sendMessage({greeting: "sn_conversation_sendmsg_request_2", result: a.data.value, sn_conversation_msg_text: request.sn_conversation_msg_text}, function(response) {});

                });

            }

        });

    }




    /*$('body').append('<div class="linkedin_lightbox_wrapper" > <span class="linkedin_lightbox_close">X</span>  <div class="linkedin_lightbox" > <img class="linkedin_lightbox_img" src="" width="100%" / ><  </div>  </div>');    


    $('body').on('click', '.linkedin_conversation_lightbox', function(e){

        e.preventDefault();

        var target_img = $(this).attr('href');
        $('.linkedin_lightbox_img').attr('src', target_img );

        $('.linkedin_lightbox_wrapper').show();

    });


    $(document).keyup(function(e) {
         if (e.key === "Escape") { // escape key maps to keycode `27`
            // <DO YOUR WORK HERE>
            $('.linkedin_lightbox_wrapper').hide();
        }
    });
    */

    // $("body").on("focusin", '.linkedin_conversation_container', function() {
    //     $("a.linkedin_conversation_lightbox").fancybox({
    //         // fancybox API options here
    //         'padding': 0
    //     }); // fancybox
    // });

});