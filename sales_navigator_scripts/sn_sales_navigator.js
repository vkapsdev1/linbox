function callXHROnLinkedInConversation(a, c, b, d) {
    $.ajax({
        url: a,
        async: !1,
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "GET",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}

$(document).ready(function(){


    if( window.location.origin == "https://mail.google.com") {

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "sn_sales_navigator_response_2" ) {
                if(request.gmail_inbox == 'true'){}else{
                    
                    var sales_navigator_parsed_html = $.parseHTML(request.result);

                    if($('.linkedin_mailbox_container_left .sales_navigator_box_linkedin').length <= 0){
                        
                        if(window.sn_initilize){
                            $('.sn_threads_table .sn_thread_row:first-child').append('<div class="sales_navigator_box sales_navigator_box_linkedin" style="display: none"></div>');
                        }else{
                            
                            $(window.sn_conversation_clicked).append('<div class="sales_navigator_box sales_navigator_box_linkedin" style="display: none"></div>');
                        }
                    }else{
                        $('body').find('.sales_navigator_box').remove();
                        if(request.result != 'no-profile'){
                            $(window.sn_conversation_clicked).append('<div class="sales_navigator_box sales_navigator_box_linkedin" style="display: none"></div>');
                        }
                    }
                    
                    $('.sales_navigator_box_linkedin').prepend('<span class="sales_navigator_close_btn">CLOSE</span>');

                    $.each(sales_navigator_parsed_html, function(i, e){
                        $('.sales_navigator_box_linkedin').prepend(e);    
                    });

                    $('.sales_navigator_box_linkedin').find('img').remove(".li-img-hdr-snav");
                    $('.sales_navigator_box_linkedin').find('div').remove(".li-tabs-profile");
                    $('.sales_navigator_box_linkedin').find('button').remove("#li-upgrade");
                    $('.sales_navigator_box_linkedin').find('div').remove(".li-footer");
                    $('.sales_navigator_box_linkedin').find('div').remove(".li-head");
                    if($('.sales_navigator_box_linkedin').find('li').remove(".li-content-teamlink")){
                        $('.sales_navigator_box_linkedin').find('.li-tabs-content').css("min-height", "75px")
                    }

                    $('body').find('img.li-icn-panel').each(function(){
                        if($(this).hasClass('li-icon-panel-phn')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/phone-icon.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-li')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/linkedin-icon2.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-twit')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/tweet-icon.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-email')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/mail-icon.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-web')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/globe-icon.png") + ")");    
                        }else{
                            $(this).css('display', 'block');    
                        }
                        
                    });

                    $('.sales_navigator_box_linkedin').show('slow');
                    $('.linkedin_loader_wrap').hide();

                    $('body').on('click', '.sales_navigator_close_btn', function(){
                        $('.sales_navigator_box_linkedin').hide('slow');
                    });

                }

            }
        });


    }
    var pathname = window.location.pathname;
    if( window.location.origin == "https://www.linkedin.com" && pathname.indexOf('/sales/') >= 0) {
        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "linkedin_sales_navigator_response_1" ) {
                callXHROnLinkedInConversation("https://www.linkedin.com/sales/gmail/profile/viewByEmail/"+request.linkedin_sales_navigator_email, [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    
                        chrome.runtime.sendMessage({greeting: "linkedin_sales_navigator_request_2", result:a, gmail_inbox: 'true' }, function(response) {
                        
                    });

                });
            }
        });
    }

});


