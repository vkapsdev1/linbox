window.linkedin_cookie = '';

function sn_getProfileDetailsForRegistration (identifier, csrf_token = null, b) {
    console.log("ocean");
    if (identifier == "undefined") {
        "function" == typeof b && b(false);
    } else {

        var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
        $.ajax({
            url: 'https://www.linkedin.com/voyager/api/identity/profiles/'+identifier+'/profileView',
            beforeSend: function(req) {
                if(csrf_token == null){
                    var csrf_token_o = document.cookie.match(JSESSIONID_REGEX)[1];    
                }else{
                    console.log(csrf_token);
                    var csrf_token_o = csrf_token;
                }  
                req.setRequestHeader('csrf-token', csrf_token_o);
            },
            xhrFields: {
                withCredentials: true
            },
            success: function(response) {
                console.log("ocean1234");
                console.log(response);
                var obj = {};
                if (response && response.profile) {
                    var profile = response.profile;
                    var entityUrn = response.entityUrn;
                    entityUrn = entityUrn.replace('urn:li:fs_profileView:', '');
                    obj = {
                        firstName: profile.firstName,
                        lastName: profile.lastName,
                        industry: profile.industryName,
                        location: profile.locationName
                    }
                    if (profile && profile.picture && profile.picture['com.linkedin.voyager.common.MediaProcessorImage'] && profile.picture['com.linkedin.voyager.common.MediaProcessorImage'].id) {
                        obj.profile_img = 'https://media.licdn.com/mpr/mpr/shrinknp_100_100' + profile.picture['com.linkedin.voyager.common.MediaProcessorImage'].id;
                    } else if (profile && profile.miniProfile && profile.miniProfile.picture && profile.miniProfile.picture['com.linkedin.common.VectorImage']) {
                        var vectorImg = profile.miniProfile.picture['com.linkedin.common.VectorImage'];
                        if (vectorImg.artifacts && vectorImg.artifacts.length > 0) {
                            obj.profile_img = vectorImg['rootUrl'] + '' + vectorImg.artifacts.splice(-1)[0].fileIdentifyingUrlPathSegment;
                        }
                    }
                    obj.linkedin_profile_id = entityUrn;
                    obj.linkedin_profile_url = 'https://www.linkedin.com/in/' + obj.linkedin_profile_id;
                    $.ajax({
                        url: 'https://www.linkedin.com/voyager/api/identity/profiles/' + identifier + '/profileContactInfo',
                        beforeSend: function(req) {
                            var csrf_token = document.cookie.match(JSESSIONID_REGEX)[1];
                            req.setRequestHeader('csrf-token', csrf_token);
                        },
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function(contact_info) {
                            if (contact_info && contact_info.emailAddress) {
                                obj.email = contact_info.emailAddress;
                                if (obj.email.indexOf('phishing') >= 0) {
                                    obj.email = decodeURIComponent(obj.email).replace(/(.*?)https:.*?=(.*)/, '$1$2');
                                }
                            }
                            /* 
                             * Put your API call here to signup to Felix with obj
                             * We can get more data, please check the response of both API calls and add more to obj
                             *
                             */
                            "function" == typeof b && b(obj)
                        }
                    });
                }
            }
        })
    }
}

$.fn.clickOff = function(callback, selfDestroy) {
    var clicked = false;
    var parent = this;
    var destroy = selfDestroy || true;
    
    parent.click(function() {
        clicked = true;
    });
    
    $(document).click(function(event) { 
        if (!clicked) {
            callback(parent, event);
        }
        clicked = false;
    });
};
function sn_listenForScrollEvent(el){
        el.on("scroll", function(){
            el.trigger("sn-scroll");
        })
    }

$(document).ready(function(){

    function callXHROnLinkedIn(a, c, b, d) {

        $.ajax({
            url: a,
            async: !1,
            beforeSend: function(a) {
                var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
                var b = document.cookie.match(JSESSIONID_REGEX)[1];

                console.log('ocean _ cookie');
                console.log(b);

                a.setRequestHeader("csrf-token", b);
                c && 0 < c.length && c.forEach(function(b) {
                    a.setRequestHeader(b.key, b.val)
                })
            },
            xhrFields: {
                withCredentials: !0
            },
            type: "GET",
            success: function(a) {
                "function" == typeof b && b(a)
            },
            complete: function(xhr){
                if(xhr.status == 401){
                    chrome.runtime.sendMessage({greeting: "sn_threads_no_request"}, function(response) { });
                }
            },
            error: function(a) {
                "function" == typeof b && b()
            }
        })
    }

    function getAMPM(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    function get_sn_thread_results(data, owner_profile_id, scroll = false){

        var sn_threads_detail = new Array();
        if(scroll){
            data.shift();
        }
        $.each(data, function(i, e){

                sn_threads_detail[i] = new Array();

                // Fetching thread details  

                sn_threads_detail[i]["subject"] = e.body;
                var participant_key = '';
                $.each(e.toGroup, function(t, g){
                    participant_key = g;
                });

                if(e.fromResolutionResult.fullName != owner_profile_id){
                    // Fetching profile name 
                    sn_threads_detail[i]["name"] = e.fromResolutionResult.fullName;

                    var identifier = e.from;
                    identifier = identifier.replace("urn:li:fs_salesProfile:(", "");
                    identifier = identifier.replace(")", "");
                    sn_threads_detail[i]["itemId"] = identifier;
                    identifier = identifier.split(',');
                    console.log(identifier);
                    sn_threads_detail[i]["publicIdentifier"] = identifier[0];

                    // Fetching profile picture
                    if(typeof e.fromResolutionResult.profilePictureDisplayImage !== "undefined"){
                        sn_threads_detail[i]["profile_pic"] = e.fromResolutionResult.profilePictureDisplayImage.artifacts[0].fileIdentifyingUrlPathSegment;
                    }else{
                        sn_threads_detail[i]["profile_pic"] = '';
                    }    
                }else{
                    
                    // Fetching profile name                    
                    sn_threads_detail[i]["name"] = e.toGroupResolutionResults[participant_key].fullName;

                    var identifier = participant_key;
                    identifier = identifier.replace("urn:li:fs_salesProfile:(", "");
                    identifier = identifier.replace(")", "");
                    sn_threads_detail[i]["itemId"] = identifier;
                    identifier = identifier.split(',');
                    sn_threads_detail[i]["publicIdentifier"] = identifier[0];
                    
                    // Fetching profile picture
                    if(typeof e.toGroupResolutionResults[participant_key].profilePictureDisplayImage !== "undefined"){
                        sn_threads_detail[i]["profile_pic"] = e.toGroupResolutionResults[participant_key].profilePictureDisplayImage.artifacts[0].fileIdentifyingUrlPathSegment;
                    }else{
                        sn_threads_detail[i]["profile_pic"] = '';
                    }    
                }
                

                sn_threads_detail[i]["occupation"] = e.toGroupResolutionResults[participant_key].headline;

                sn_threads_detail[i]["createdAt"] = e.createdDate;

                // Fetching conversation_id
                var conversations_id = e.entityUrn;
                sn_threads_detail[i]["conversations_id"] = conversations_id.replace("urn:li:fs_salesMailboxThread:", "");

                // Fetching mseeage type
                if(e.itemType == "INMAIL" || e.itemType == "SPONSORED_INMAIL"){
                    sn_threads_detail[i]["inmails"] = true;
                }else{
                    sn_threads_detail[i]["inmails"] = false;
                }

                if( e.read == false ){
                    sn_threads_detail[i]["font_weight"] = "bold";
                }else{
                    sn_threads_detail[i]["font_weight"] = "normal";
                }

                sn_threads_detail[i]["seenAt"] = '';   
                

        });

        return sn_threads_detail;
    }

    

    if( window.location.origin == "https://mail.google.com") {

        var check_gmailbody_load = setInterval(function(){ 

            if($('body').find('.aim').length > 0){

                clearInterval(check_gmailbody_load);
                console.log("check_gmailbody_load off");
            
                $('h2').each(function(){
                    if($(this).text() == "Labels"){
                        $(this).parent().find('a').each(function(){
                            if($(this).text() == "Inbox"){
                                $(this).parents().eq(5).append("<div class='aim' id='sn_label_container'><div class='TO'><div class='TN aHS-bnw' style='margin-left:0px'><img src='"+chrome.runtime.getURL("images/linkedin-icon.png")+"' class='linkedin_mailbox_logo' /><span class='nU' id='sn_label'><a href='javascript:void(0)' class='n0'>Sales Navigator Inbox</a></span><div class='linkedin_unread_count'></div></div></div>");
                            }
                        });
                    }
                });

                $('.aim').on('click', function(){
                    
                    if($(this).attr('id') == "sn_label_container" || $(this).attr('id') == "linkedin_label_container" ){
                        
                        $('.aim').removeClass('ain');
                        $('.aim .TO').removeClass('nZ aiq');
                        $(this).addClass('ain');
                        $(this).find('.TO').addClass('nZ aiq');
                        $(".bkK").hide();
                        $(".linkedin_mailbox").show();

                    }else{

                        $('#sn_label_container').removeClass('ain');
                        $('#sn_label_container').find('.TO').removeClass('nZ aiq');
                        $(".bkK").show();
                        $(".linkedin_mailbox").hide();

                    }
                });

                // var lastTimeForEachPage = new Map();

                $('body').find('#sn_label').click(function(){
                    
                    chrome.runtime.sendMessage({greeting: "sn_threads_request_1"}, function(response) {
                     });

                    window.sn_initilize = true;

                    chrome.storage.local.get(null, function(items){
                        var allKeys   = Object.keys(items);
                        $.each(allKeys, function(i, e){
                            if(e.indexOf('attachment_urn') >= 0 ){
                                chrome.storage.local.remove([e]);
                            }
                        });
                    });

                    // chrome.runtime.sendMessage({greeting: "linkedin_threads_unread_count_request_1"}, function(response) { });

                    if($('.linkedin_mailbox').length <= 0){
                        var linkedin_mailbox_style = $('body').find('.bkK').attr('style');
                        $('body').find('.bkK').hide();
                        $('body').find('.bkK').after("<div class='bKK linkedin_mailbox'><div class='linkedin_mailbox_container_left sn_mailbox_container_left'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div><div class='linkedin_mailbox_container'></div></div><div class='linkedin_mailbox_container_right'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div><div class='linkedin_mailbox_container'></div></div></div>");
                        // window.page_no = 1;
                        
                        // lastTimeForEachPage.set(window.page_no, 9999999999000);
                    }

                    $('.linkedin_mailbox_container_left').addClass('sn_mailbox_container_left');

                    $('.linkedin_mailbox_container_left .linkedin_loader_wrap').show();

                    $('.linkedin_mailbox_container_right .linkedin_loader_wrap').show();

                    // $('.linkedin_mailbox_back_btn').hide();

                    sn_listenForScrollEvent($(".sn_mailbox_container_left"));

                    $('body').on('sn-scroll', '.sn_mailbox_container_left' , function(e){

                        if($('.linkedin_mailbox_container_left').length > 0){

                            if($(this).scrollTop() >= $('.sn_threads_table').height() - $(this).height()){

                                if(window.sn_thread_repeat){
                                    window.sn_thread_repeat = false;
                                    

                                    $('.linkedin_mailbox_container_left .linkedin_loader_wrap').show();

                                    var most_recent_thread_timestamp = $('.sn_threads_table .sn_thread_row:last-child').data('timestamp'); 
                                    
                                    chrome.runtime.sendMessage({greeting: "sn_threads_scroll_request_1", thread_timestamp: most_recent_thread_timestamp}, function(response) { 
                                        
                                    });
                                }

                            }
                        }

                      
                    });

                });

                chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

                        if(request.ocean == "sn_threads_no_response"){
                            $('.linkedin_loader_wrap').hide();

                            if($('.sn_mailbox_popup').length > 0){
                                $('.sn_mailbox_popup').show();
                            }else{
                                $('body').append("<div class='sn_mailbox_popup'><div class='linkedin_popup_box'><h3>Please Login in your Sales Navigator account</h3><a class='linkedin_login_btn' href='https://www.linkedin.com/sales/login' target='_blank'><img src='"+chrome.runtime.getURL("images/linkedin-icon2.png")+"'> Login</a></div></div>");    
                            }

                            $('.linkedin_popup_box').clickOff(function() {
                                $('.sn_mailbox_popup').hide();
                            });
                            
                        }


                        if( request.ocean == "sn_threads_response_2") {
                            console.log(window.linkedin_cookie);
                            $('.linkedin_mailbox_container_left .linkedin_loader_wrap').hide();

                            console.log(request);

                            window.sn_thread_repeat = true;

                            var sn_threads_detail = get_sn_thread_results(request.result.elements, request.owner_profile_id);

                            console.log(sn_threads_detail);

                            var totalMessages = request.result.paging.total;                                
                            var totalPage = (totalMessages - totalMessages % 20) / 20 + 1;
                            
                            if(window.totalPage === undefined){
                                window.totalPage = totalPage;
                            }else if(totalPage > window.totalPage){
                                window.totalPage = totalPage;
                            }                         

                            $('.linkedin_mailbox_pagination_numbers').html(window.page_no + " of " +window.totalPage);

                            var sn_threads_html = "<div class='sn_threads_table'>";

                            $.each(sn_threads_detail, function(g, h){

                                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                
                                var dateObj = new Date(h.createdAt);
                                var time = getAMPM(dateObj);
                                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                                var day = dateObj.getUTCDate();
                                var year = dateObj.getUTCFullYear();


                                var c_dateObj = new Date();
                                var c_month = c_dateObj.getUTCMonth() + 1; //months from 1-12
                                var c_day = c_dateObj.getUTCDate();
                                var c_year = c_dateObj.getUTCFullYear();

                                if(c_day+"/"+c_month+"/"+c_year == day+"/"+month+"/"+year){
                                    var createdAt = time;
                                }else{
                                    var createdAt = + day +" "+months[month-1];
                                }

                                var profile_pic = h.profile_pic;

                                if(profile_pic === ""){
                                    profile_pic = chrome.runtime.getURL("images/no-profile.png");
                                }

                                if(typeof h.occupation !== "undefined"){
                                    occupation = h.occupation;
                                }else{
                                    occupation = "";
                                }

                                if(typeof h.seenAt !== "undefined"){
                                    seenAt = h.seenAt;
                                }else{
                                    seenAt = "";
                                }

                                if(h.font_weight == "normal"){
                                    var read_class = "sn_thread_row_normal";
                                }else{
                                    var read_class = "sn_thread_row_bold";
                                }
                                
                                sn_threads_html += '<div class="sn_thread_row '+read_class+'" data-conversation_id="'+h.conversations_id+'" data-timestamp="'+h.createdAt+'" data-occupation="'+occupation+'" data-title="'+h.name+'" data-publicIdentifier="'+h.publicIdentifier+'" data-itemid="'+h.itemId+'" data-profile_pic="'+profile_pic+'" data-seenAt="'+seenAt+'" style="font-weight: '+h.font_weight+'"><div class="sn_thread_profile_pic"><img src="'+profile_pic+'" /></div><div class="sn_thread_profile_nmd"><div class="sn_thread_name"><div class="thread-name">'+h.name+'</div>  </div>';

                                sn_threads_html += "<tr><td class='sn_thread_subject' colspan='3'>";

                                if(h.inmails){
                                    sn_threads_html += "<div class='thread-subject linkedin_thread_subject_text'><strong>InMail - </strong>"+h.subject
                                }else{
                                    sn_threads_html += '<div class="thread-subject sn_attach_subject sn_thread_subject_text">';

                                    if(h.subject == "Attachment"){
                                        sn_threads_html += '<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false"><path d="M5,2h5.5c3,0,5.5,2.5,5.5,5.5S13.5,13,10.5,13H4c-2.2,0-4-1.8-4-4s1.8-4,4-4h5.5C10.9,5,12,6.1,12,7.5S10.9,10,9.5,10H5V8.1h4.5c0.3,0,0.6-0.3,0.6-0.6c0,0,0,0,0,0c0-0.3-0.3-0.6-0.6-0.6c0,0,0,0,0,0H4C2.9,6.8,1.9,7.6,1.9,8.7c0,0.1,0,0.2,0,0.3c-0.1,1.1,0.8,2.1,1.9,2.1c0.1,0,0.2,0,0.2,0h6.6c1.9,0,3.5-1.6,3.5-3.5c0,0,0-0.1,0-0.1c0.1-1.9-1.5-3.6-3.4-3.6c0,0,0,0-0.1,0H5V2z" class="small-icon" style="fill-opacity: 1"></path></svg>';    
                                    }
                                    sn_threads_html += h.subject;
                                }

                                sn_threads_html += '</div></div><div class="sn_thread_date">'+createdAt+'</div></div>';

                                if(g == 0){
                                    chrome.runtime.sendMessage({greeting: "sn_conversation_request_1", sn_conversation_id: h.conversations_id, sn_conversation_title: h.name, sn_conversation_image: profile_pic, sn_conversation_occupation: occupation, sn_conversation_seenAt: seenAt, sn_conversation_publicIdentifier: h.publicIdentifier, sn_conversation_itemId: h.itemId}, function(response) {

                                    });
                                }

                            });
                            
                            sn_threads_html += "</div>";

                            $('body').find('.linkedin_mailbox_container_left .linkedin_mailbox_container').html(sn_threads_html);

                         }

                         if(request.ocean == "sn_threads_scroll_response_2"){

                            console.log(request.result);

                            $('.linkedin_mailbox_container_left .linkedin_loader_wrap').hide();

                            var sn_threads_detail = get_sn_thread_results(request.result.elements, request.owner_profile_id, true);

                            console.log(sn_threads_detail);
                            var totalMessages = request.result.paging.total;
                            var totalPage = (totalMessages - totalMessages % 20) / 20 + 1;
                            
                            if(window.totalPage === undefined){
                                window.totalPage = totalPage;
                            }else if(totalPage > window.totalPage){
                                window.totalPage = totalPage;
                            }                         

                            $('.linkedin_mailbox_pagination_numbers').html(window.page_no + " of " +window.totalPage);

                            var linkedin_threads_html = "";

                            $.each(sn_threads_detail, function(g, h){

                                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                
                                var dateObj = new Date(h.createdAt);
                                var time = getAMPM(dateObj);
                                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                                var day = dateObj.getUTCDate();
                                var year = dateObj.getUTCFullYear();


                                var c_dateObj = new Date();
                                var c_month = c_dateObj.getUTCMonth() + 1; //months from 1-12
                                var c_day = c_dateObj.getUTCDate();
                                var c_year = c_dateObj.getUTCFullYear();

                                if(c_day+"/"+c_month+"/"+c_year == day+"/"+month+"/"+year){
                                    var createdAt = time;
                                }else{
                                    var createdAt = + day +" "+months[month-1];
                                }

                                var profile_pic = h.profile_pic;

                                if(profile_pic === ""){
                                    profile_pic = chrome.runtime.getURL("images/no-profile.png");
                                }

                                if(typeof h.occupation !== "undefined"){
                                    occupation = h.occupation;
                                }else{
                                    occupation = "";
                                }

                                if(typeof h.seenAt !== "undefined"){
                                    seenAt = h.seenAt;
                                }else{
                                    seenAt = "";
                                }

                                if(h.font_weight == "normal"){
                                    var read_class = "sn_thread_row_normal";
                                }else{
                                    var read_class = "sn_thread_row_bold";
                                }
                                
                                sn_threads_html += '<div class="sn_thread_row '+read_class+'" data-conversation_id="'+h.conversations_id+'" data-timestamp="'+h.createdAt+'" data-occupation="'+occupation+'" data-title="'+h.name+'" data-publicIdentifier="'+h.publicIdentifier+'" data-itemid="'+h.itemId+'" data-profile_pic="'+profile_pic+'" data-seenAt="'+seenAt+'" style="font-weight: '+h.font_weight+'"><div class="sn_thread_profile_pic"><img src="'+profile_pic+'" /></div><div class="sn_thread_profile_nmd"><div class="sn_thread_name"><div class="thread-name">'+h.name+'</div>  </div>';

                                sn_threads_html += "<tr><td class='sn_thread_subject' colspan='3'>";

                                if(h.inmails){
                                    sn_threads_html += "<div class='thread-subject linkedin_thread_subject_text'><strong>InMail - </strong>"+h.subject
                                }else{
                                    sn_threads_html += '<div class="thread-subject sn_attach_subject sn_thread_subject_text">';

                                    if(h.subject == "Attachment"){
                                        sn_threads_html += '<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false"><path d="M5,2h5.5c3,0,5.5,2.5,5.5,5.5S13.5,13,10.5,13H4c-2.2,0-4-1.8-4-4s1.8-4,4-4h5.5C10.9,5,12,6.1,12,7.5S10.9,10,9.5,10H5V8.1h4.5c0.3,0,0.6-0.3,0.6-0.6c0,0,0,0,0,0c0-0.3-0.3-0.6-0.6-0.6c0,0,0,0,0,0H4C2.9,6.8,1.9,7.6,1.9,8.7c0,0.1,0,0.2,0,0.3c-0.1,1.1,0.8,2.1,1.9,2.1c0.1,0,0.2,0,0.2,0h6.6c1.9,0,3.5-1.6,3.5-3.5c0,0,0-0.1,0-0.1c0.1-1.9-1.5-3.6-3.4-3.6c0,0,0,0-0.1,0H5V2z" class="small-icon" style="fill-opacity: 1"></path></svg>';    
                                    }
                                    sn_threads_html += h.subject;
                                }

                                sn_threads_html += '</div></div><div class="sn_thread_date">'+createdAt+'</div></div>';

                                // if(g == 0){
                                //     chrome.runtime.sendMessage({greeting: "sn_conversation_request_1", sn_conversation_id: h.conversations_id, sn_conversation_title: h.name, sn_conversation_image: profile_pic, sn_conversation_occupation: occupation, sn_conversation_seenAt: seenAt, sn_conversation_publicIdentifier: h.publicIdentifier}, function(response) {

                                //     });
                                // }

                            });
                            
                            //$('body').find('.linkedin_threads_table').html(linkedin_threads_html);
                            $('body').find('.sn_threads_table').append(linkedin_threads_html);

                            window.sn_thread_repeat = true;

                         }

                         // if(request.ocean == "linkedin_threads_unread_count_response_2"){
                         //    console.log("linkedin unread count");
                         //    console.log(request.linkedin_unread_count);

                         //    $.each(request.linkedin_unread_count.elements ,function(i, e){
                         //        if(e.tab == "MESSAGING"){
                         //            if(e.count != 0){
                         //                $('.linkedin_unread_count').html(e.count);
                         //            }
                         //        }
                         //    });

                         // }

                    });



                // $('body').on('click', '.linkedin_mailbox_back_btn', function(){
                //     $('body').find('#linkedin_label').click();
                // });

                


            //}, 6000);

        }else{
                console.log("check_gmailbody_load on");
            }
        }, 1000);
        
    }

    var pathname = window.location.pathname;
    if( window.location.origin == "https://www.linkedin.com" && pathname.indexOf('/sales/') >= 0) {

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "sn_threads_response_1") {

                var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
                var b = document.cookie.match(JSESSIONID_REGEX)[1];

                window.linkedin_cookie = b;

                sn_getProfileDetailsForRegistration ('me', null, function(res) {
                    
                    callXHROnLinkedIn('https://www.linkedin.com/sales-api/salesApiMailbox?decoration=%28entityUrn%2CuniqueId%2Csubject%2Cbody%2CblockCopy%2CfooterText%2Cread%2CitemType%2CcustomPropertyType%2CsalesMessageType%2CitemStatus%2Creplied%2Cinbox%2Cpending%2Carchived%2CcreatedDate%2ClastModified%2Cattachments*%28name%2CmediaId%2CmimeType%2Csize%2Creference%2CattachmentUrn%29%2Cfrom~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%29%2CtoGroup*~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%29%29&count=20&createdBefore='+new Date().getTime()+'&keyword='+res.firstName+' '+res.lastName+'&q=searchMailboxThreads', [{
                        key: "x-restli-protocol-version",
                        val: "2.0.0"
                    }], function(a) {
                        a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");
                        var profile_id = res.firstName+' '+res.lastName;
                        chrome.runtime.sendMessage({greeting: "sn_threads_request_2", result:a, owner_profile_id:  profile_id}, function(response) {console.log('linkedin_threads calling2');});

                    });

                });

            }
            if(request.ocean == "sn_threads_scroll_response_1"){

                sn_getProfileDetailsForRegistration ('me', null, function(res) {

                    callXHROnLinkedIn('https://www.linkedin.com/sales-api/salesApiMailbox?decoration=%28entityUrn%2CuniqueId%2Csubject%2Cbody%2CblockCopy%2CfooterText%2Cread%2CitemType%2CcustomPropertyType%2CsalesMessageType%2CitemStatus%2Creplied%2Cinbox%2Cpending%2Carchived%2CcreatedDate%2ClastModified%2Cattachments*%28name%2CmediaId%2CmimeType%2Csize%2Creference%2CattachmentUrn%29%2Cfrom~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%29%2CtoGroup*~fs_salesProfile%28entityUrn%2CfirstName%2ClastName%2CfullName%2Cheadline%2CpictureInfo%2CprofilePictureDisplayImage%29%29&count=20&createdBefore='+new Date().getTime()+'&keyword='+res.firstName+' '+res.lastName+'&q=searchMailboxThreads', [{
                        key: "x-restli-protocol-version",
                        val: "2.0.0"
                    }], function(a) {
                        a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");
                        var profile_id = res.firstName+' '+res.lastName;
                        chrome.runtime.sendMessage({greeting: "sn_threads_scroll_request_2", result:a, owner_profile_id:  profile_id }, function(response) {
                        });

                    });

                });

            }

            // if(request.ocean == "linkedin_threads_unread_count_response_1"){

            //     callXHROnLinkedIn("https://www.linkedin.com/voyager/api/voyagerCommunicationsTabBadges?q=tabBadges&countFrom=0", [{
            //         key: "x-restli-protocol-version",
            //         val: "2.0.0"
            //     }], function(a) {
            //         console.log(a);
            //         // a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");

            //         chrome.runtime.sendMessage({greeting: "linkedin_threads_unread_count_request_2", result:a }, function(response) {
            //         });

            //     });

            // }   
        });
        
    }


    

});
