$.fn.clickOff = function(callback, selfDestroy) {
    var clicked = false;
    var parent = this;
    var destroy = selfDestroy || true;
    
    parent.click(function() {
        clicked = true;
    });
    
    $(document).click(function(event) { 
        if (!clicked) {
            callback(parent, event);
        }
        clicked = false;
    });
};
function listenForScrollEvent(el){
        el.on("scroll", function(){
            el.trigger("custom-scroll");
        })
    }

$(document).ready(function(){

    function callXHROnLinkedIn(a, c, b, d) {

        $.ajax({
            url: a,
            async: !1,
            beforeSend: function(a) {
                var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
                var b = document.cookie.match(JSESSIONID_REGEX)[1];
                a.setRequestHeader("csrf-token", b);
                c && 0 < c.length && c.forEach(function(b) {
                    a.setRequestHeader(b.key, b.val)
                })
            },
            xhrFields: {
                withCredentials: !0
            },
            type: "GET",
            success: function(a) {
                "function" == typeof b && b(a)
            },
            complete: function(xhr){
                if(xhr.status == 401){
                    chrome.runtime.sendMessage({greeting: "linkedin_threads_no_request"}, function(response) { });
                }
            },
            error: function(a) {
                "function" == typeof b && b()
            }
        })
    }

    function getAMPM(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    function get_thread_results(data, scroll = false){

        var linkedin_threads_detail = new Array();
        if(scroll){
            data.shift();
        }
        $.each(data, function(i, e){

                linkedin_threads_detail[i] = new Array();

                // Fetching thread details  

                $.each(e.participants, function(a, b){

                    if(b["com.linkedin.voyager.messaging.MessagingMember"] !== undefined){


                        // Fetching name
                        var name_flag = true;
                        if(b["com.linkedin.voyager.messaging.MessagingMember"].alternateName !== undefined && name_flag){
                            
                            linkedin_threads_detail[i]["name"] = b["com.linkedin.voyager.messaging.MessagingMember"].alternateName;

                            name_flag = false;

                        }
                        if(b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile !== undefined && name_flag){

                            linkedin_threads_detail[i]["name"] = b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.firstName +" "+ b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.lastName;

                            linkedin_threads_detail[i]["publicIdentifier"] = b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.publicIdentifier;

                        }

                        // Fetching profile picture
                        var picture_flag = true;
                        if(b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.picture !== undefined && picture_flag){

                            linkedin_threads_detail[i]["profile_pic"] = b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.picture["com.linkedin.common.VectorImage"].rootUrl + b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.picture["com.linkedin.common.VectorImage"].artifacts[0].fileIdentifyingUrlPathSegment;

                            picture_flag = false;

                        }
                        if(b["com.linkedin.voyager.messaging.MessagingMember"].alternateImage !== undefined && picture_flag){

                            linkedin_threads_detail[i]["profile_pic"] = b["com.linkedin.voyager.messaging.MessagingMember"].alternateImage["com.linkedin.common.VectorImage"].artifacts[0].fileIdentifyingUrlPathSegment;

                        }

                        // Fetching occupation
                        if(b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.occupation !== undefined){
                            linkedin_threads_detail[i]["occupation"] = b["com.linkedin.voyager.messaging.MessagingMember"].miniProfile.occupation;                            
                        }

                    }

                });

                $.each(e.events, function(c, d){

                    // Fetching subject
                    var subject_flag = true;

                    if(d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].attachments !== undefined && subject_flag){
                        var attachments  = d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].attachments;
                        if(attachments.length > 0){
                            linkedin_threads_detail[i]["subject"] =  "Attachment";

                            subject_flag = false;    
                        }                        
                    }                    

                    if(d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].subject !== undefined && subject_flag){

                        linkedin_threads_detail[i]["subject"] =  d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].subject;

                        subject_flag = false;
                    }
                    if(d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].body !== undefined && subject_flag && d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].body != ""){

                        linkedin_threads_detail[i]["subject"] =  d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].body;

                        subject_flag = false;
                    }

                    if(typeof d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].attributedBody.text !== "undefined" && subject_flag){
                        
                        linkedin_threads_detail[i]["subject"] = d.eventContent["com.linkedin.voyager.messaging.event.MessageEvent"].attributedBody.text;

                        subject_flag = false;   

                    }

                    // Fetching timestamp
                    linkedin_threads_detail[i]["createdAt"] = d.createdAt;

                    // Fetching mseeage type
                    if(d.subtype == "INMAIL" || d.subtype == "SPONSORED_INMAIL"){
                        linkedin_threads_detail[i]["inmails"] = true;
                    }else{
                        linkedin_threads_detail[i]["inmails"] = false;
                    }

                });

                // Fetching conversation_id
                var conversations_id = e.entityUrn;
                linkedin_threads_detail[i]["conversations_id"] = conversations_id.replace("urn:li:fs_conversation:", "");

                if( e.read === false ){
                    linkedin_threads_detail[i]["font_weight"] = "bold";
                }else{
                    linkedin_threads_detail[i]["font_weight"] = "normal";
                }

                if(typeof e.receipts !== "undefined"){
                    $.each(e.receipts, function(r,s){
                        if(typeof s.seenReceipt !== "undefined"){
                            linkedin_threads_detail[i]["seenAt"] = s.seenReceipt.seenAt;   
                        }
                    });
                    
                }

        });

        return linkedin_threads_detail;
    }

    

    if( window.location.origin == "https://mail.google.com") {

        var check_gmailbody_load = setInterval(function(){ 

            if($('body').find('.aim').length > 0){

                clearInterval(check_gmailbody_load);
                console.log("check_gmailbody_load off");
            
                $('h2').each(function(){
                    if($(this).text() == "Labels"){
                        $(this).parent().find('a').each(function(){
                            if($(this).text() == "Inbox"){
                                $(this).parents().eq(5).append("<div class='aim' id='linkedin_label_container'><div class='TO'><div class='TN aHS-bnw' style='margin-left:0px'><img src='"+chrome.runtime.getURL("images/linkedin-icon.png")+"' class='linkedin_mailbox_logo' /><span class='nU' id='linkedin_label'><a href='javascript:void(0)' class='n0'>LinkedIn Inbox</a></span><div class='linkedin_unread_count'></div></div></div>");
                            }
                        });
                    }
                });

                var $span = $(".n3 div");
                $span.replaceWith(function () {
                    return $('<span/>', {
                        class: 'byl',
                        html: this.innerHTML
                    });
                });
                
                $('.aim').on('click', function(){
                    
                    if($(this).attr('id') == "linkedin_label_container"){
                        
                        $('.aim').removeClass('ain');
                        $('.aim .TO').removeClass('nZ aiq');
                        $(this).addClass('ain');
                        $(this).find('.TO').addClass('nZ aiq');
                        $(".bkK").hide();
                        $(".linkedin_mailbox").show();

                    }else{

                        $('#linkedin_label_container').removeClass('ain');
                        $('#linkedin_label_container').find('.TO').removeClass('nZ aiq');
                        $(".bkK").show();
                        $(".linkedin_mailbox").hide();

                    }
                });

                var lastTimeForEachPage = new Map();

                $('body').find('#linkedin_label').click(function(){
                    
                    chrome.runtime.sendMessage({greeting: "linkedin_threads_request_1"}, function(response) {
                     });

                    window.initilize = true;

                    chrome.storage.local.get(null, function(items){
                        var allKeys   = Object.keys(items);
                        $.each(allKeys, function(i, e){
                            if(e.indexOf('attachment_urn') >= 0 ){
                                chrome.storage.local.remove([e]);
                            }
                        });
                    });

                    chrome.runtime.sendMessage({greeting: "linkedin_threads_unread_count_request_1"}, function(response) { });

                    if($('.linkedin_mailbox').length <= 0){
                        var linkedin_mailbox_style = $('body').find('.bkK').attr('style');
                        $('body').find('.bkK').hide();
                        $('body').find('.bkK').after("<div class='bKK linkedin_mailbox'><div class='linkedin_mailbox_container_left'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div><div class='linkedin_mailbox_container'></div></div><div class='linkedin_mailbox_container_right'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div><div class='linkedin_mailbox_container'></div></div></div>");
                        window.page_no = 1;
                        
                        lastTimeForEachPage.set(window.page_no, 9999999999000);
                    }
                    $('.linkedin_mailbox_container_left').removeClass('sn_mailbox_container_left');
                    $('.linkedin_mailbox_container_left .linkedin_loader_wrap').show();

                    $('.linkedin_mailbox_container_right .linkedin_loader_wrap').show();

                    $('.linkedin_mailbox_back_btn').hide();

                    listenForScrollEvent($(".linkedin_mailbox_container_left"));

                    $('body').on('custom-scroll', '.linkedin_mailbox_container_left' , function(e){

                        if($('.linkedin_mailbox_container_left').length > 0){

                            if($(this).scrollTop() >= $('.linkedin_threads_table').height() - $(this).height()){

                                if(window.thread_repeat){
                                    window.thread_repeat = false;
                                    

                                    $('.linkedin_mailbox_container_left .linkedin_loader_wrap').show();

                                    var most_recent_thread_timestamp = $('.linkedin_threads_table .linkedin_thread_row:last-child').data('timestamp'); 
                                    
                                    chrome.runtime.sendMessage({greeting: "linkedin_threads_scroll_request_1", thread_timestamp: most_recent_thread_timestamp}, function(response) { 
                                        
                                    });
                                }

                            }
                        }

                      
                    });

                });

                $('body').on('click', '.linkedin_mailbox_pagination_next', function(){

                    

                    $('.linkedin_mailbox_container_left .linkedin_loader_wrap').show();

                    $('.linkedin_mailbox_container_right .linkedin_loader_wrap').show();

                    var most_recent_thread_timestamp = $('.linkedin_threads_table .linkedin_thread_row:last-child').data('timestamp');

                    var prev_thread_timestamp = $('.linkedin_threads_table .linkedin_thread_row:first-child').data('timestamp'); 

                    lastTimeForEachPage.set(window.page_no, prev_thread_timestamp);
                    
                    window.page_no = window.page_no + 1;
                    
                    chrome.runtime.sendMessage({greeting: "linkedin_threads_scroll_request_1", thread_timestamp: most_recent_thread_timestamp}, function(response) { 
                        
                    });
                    

                });

                $('body').on('click', '.linkedin_mailbox_pagination_prev', function(){

                    if(window.page_no != 1){

                        $('.linkedin_mailbox_container_left .linkedin_loader_wrap').show();

                        $('.linkedin_mailbox_container_right .linkedin_loader_wrap').show();

                        window.page_no = window.page_no - 1;
                        
                        var most_recent_thread_timestamp = lastTimeForEachPage.get(window.page_no);
                        
                        chrome.runtime.sendMessage({greeting: "linkedin_threads_scroll_request_1", thread_timestamp: most_recent_thread_timestamp}, function(response) { 
                            
                        });                    
                    }

                });

                chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

                        if(request.ocean == "linkedin_threads_no_response"){
                            $('.linkedin_loader_wrap').hide();

                            if($('.linkedin_mailbox_popup').length > 0){
                                $('.linkedin_mailbox_popup').show();
                            }else{
                                $('body').append("<div class='linkedin_mailbox_popup'><div class='linkedin_popup_box'><h3>Please Login in your LinkedIn account</h3><a class='linkedin_login_btn' href='https://www.linkedin.com/feed/' target='_blank'><img src='"+chrome.runtime.getURL("images/linkedin-icon2.png")+"'> Login</a></div></div>");    
                            }

                            $('.linkedin_popup_box').clickOff(function() {
                                $('.linkedin_mailbox_popup').hide();
                            });
                            
                        }


                        if( request.ocean == "linkedin_threads_response_2") {

                            $('.linkedin_mailbox_container_left .linkedin_loader_wrap').hide();

                            console.log(request.result);

                            window.thread_repeat = true;

                            var linkedin_threads_detail = get_thread_results(request.result.elements);

                            console.log(linkedin_threads_detail);

                            var totalMessages = request.result.paging.total;                                
                            var totalPage = (totalMessages - totalMessages % 20) / 20 + 1;
                            
                            if(window.totalPage === undefined){
                                window.totalPage = totalPage;
                            }else if(totalPage > window.totalPage){
                                window.totalPage = totalPage;
                            }                         

                            $('.linkedin_mailbox_pagination_numbers').html(window.page_no + " of " +window.totalPage);

                            var linkedin_threads_html = "<div class='linkedin_threads_table'>";

                            $.each(linkedin_threads_detail, function(g, h){

                                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                
                                var dateObj = new Date(h.createdAt);
                                var time = getAMPM(dateObj);
                                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                                var day = dateObj.getUTCDate();
                                var year = dateObj.getUTCFullYear();


                                var c_dateObj = new Date();
                                var c_month = c_dateObj.getUTCMonth() + 1; //months from 1-12
                                var c_day = c_dateObj.getUTCDate();
                                var c_year = c_dateObj.getUTCFullYear();

                                if(c_day+"/"+c_month+"/"+c_year == day+"/"+month+"/"+year){
                                    var createdAt = time;
                                }else{
                                    var createdAt = + day +" "+months[month-1];
                                }

                                var profile_pic = h.profile_pic;

                                if(typeof profile_pic === "undefined"){
                                    profile_pic = chrome.runtime.getURL("images/no-profile.png");
                                }

                                if(typeof h.occupation !== "undefined"){
                                    occupation = h.occupation;
                                }else{
                                    occupation = "";
                                }

                                if(typeof h.seenAt !== "undefined"){
                                    seenAt = h.seenAt;
                                }else{
                                    seenAt = "";
                                }

                                if(h.font_weight == "normal"){
                                    var read_class = "linkedin_thread_row_normal";
                                }else{
                                    var read_class = "linkedin_thread_row_bold";
                                }
                                
                                linkedin_threads_html += '<div class="linkedin_thread_row" '+read_class+' data-conversation_id="'+h.conversations_id+'" data-timestamp="'+h.createdAt+'" data-occupation="'+occupation+'" data-title="'+h.name+'" data-publicIdentifier="'+h.publicIdentifier+'" data-profile_pic="'+profile_pic+'" data-seenAt="'+seenAt+'" style="font-weight: "'+h.font_weight+'"><div class="linkedin_thread_profile_pic"><img src="'+profile_pic+'" /></div><div class="linkedin_thread_profile_nmd"><div class="linkedin_thread_name"><div class="thread-name">'+h.name+'</div>  </div>';

                                linkedin_threads_html += "<tr><td class='linkedin_thread_subject' colspan='3'>";

                                if(h.inmails){
                                    linkedin_threads_html += "<div class='thread-subject linkedin_thread_subject_text'><strong>InMail - </strong>"+h.subject
                                }else{
                                    linkedin_threads_html += '<div class="thread-subject linkedin_attach_subject linkedin_thread_subject_text">';

                                    if(h.subject == "Attachment"){
                                        linkedin_threads_html += '<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false"><path d="M5,2h5.5c3,0,5.5,2.5,5.5,5.5S13.5,13,10.5,13H4c-2.2,0-4-1.8-4-4s1.8-4,4-4h5.5C10.9,5,12,6.1,12,7.5S10.9,10,9.5,10H5V8.1h4.5c0.3,0,0.6-0.3,0.6-0.6c0,0,0,0,0,0c0-0.3-0.3-0.6-0.6-0.6c0,0,0,0,0,0H4C2.9,6.8,1.9,7.6,1.9,8.7c0,0.1,0,0.2,0,0.3c-0.1,1.1,0.8,2.1,1.9,2.1c0.1,0,0.2,0,0.2,0h6.6c1.9,0,3.5-1.6,3.5-3.5c0,0,0-0.1,0-0.1c0.1-1.9-1.5-3.6-3.4-3.6c0,0,0,0-0.1,0H5V2z" class="small-icon" style="fill-opacity: 1"></path></svg>';    
                                    }
                                    linkedin_threads_html += h.subject;
                                }

                                linkedin_threads_html += '</div></div><div class="linkedin_thread_date">'+createdAt+'</div></div>';

                                if(g == 0){
                                    console.log('linkedin_conversation_request_1');
                                    chrome.runtime.sendMessage({greeting: "linkedin_conversation_request_1", linkedin_conversation_id: h.conversations_id, linkedin_conversation_title: h.name, linkedin_conversation_image: profile_pic, linkedin_conversation_occupation: occupation, linkedin_conversation_seenAt: seenAt, linkedin_conversation_publicIdentifier: h.publicIdentifier}, function(response) {

                                    });
                                }

                            });
                            
                            linkedin_threads_html += "</div>";

                            $('body').find('.linkedin_mailbox_container_left .linkedin_mailbox_container').html(linkedin_threads_html);

                         }

                         if(request.ocean == "linkedin_threads_scroll_response_2"){

                            console.log(request.result);

                            $('.linkedin_mailbox_container_left .linkedin_loader_wrap').hide();

                            var linkedin_threads_detail = get_thread_results(request.result.elements, true);

                            console.log(linkedin_threads_detail);
                            var totalMessages = request.result.paging.total;
                            var totalPage = (totalMessages - totalMessages % 20) / 20 + 1;
                            
                            if(window.totalPage === undefined){
                                window.totalPage = totalPage;
                            }else if(totalPage > window.totalPage){
                                window.totalPage = totalPage;
                            }                         

                            $('.linkedin_mailbox_pagination_numbers').html(window.page_no + " of " +window.totalPage);

                            var linkedin_threads_html = "";

                            $.each(linkedin_threads_detail, function(g, h){

                                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                
                                var dateObj = new Date(h.createdAt);
                                var time = getAMPM(dateObj);
                                var month = dateObj.getUTCMonth() + 1; //months from 1-12
                                var day = dateObj.getUTCDate();
                                var year = dateObj.getUTCFullYear();


                                var c_dateObj = new Date();
                                var c_month = c_dateObj.getUTCMonth() + 1; //months from 1-12
                                var c_day = c_dateObj.getUTCDate();
                                var c_year = c_dateObj.getUTCFullYear();

                                if(c_day+"/"+c_month+"/"+c_year == day+"/"+month+"/"+year){
                                    var createdAt = time;
                                }else{
                                    var createdAt = + day +" "+months[month-1];
                                }

                                var profile_pic = h.profile_pic;

                                if(typeof profile_pic === "undefined"){
                                    profile_pic = chrome.runtime.getURL("images/no-profile.png");
                                }

                                if(typeof h.occupation !== "undefined"){
                                    occupation = h.occupation;
                                }else{
                                    occupation = "";
                                }

                                if(typeof h.seenAt !== "undefined"){
                                    seenAt = h.seenAt;
                                }else{
                                    seenAt = "";
                                }

                                if(h.font_weight == "normal"){
                                    var read_class = "linkedin_thread_row_normal";
                                }else{
                                    var read_class = "linkedin_thread_row_bold";
                                }

                                linkedin_threads_html += '<div class="linkedin_thread_row" '+read_class+' data-conversation_id="'+h.conversations_id+'" data-timestamp="'+h.createdAt+'" data-occupation="'+occupation+'" data-title="'+h.name+'" data-publicIdentifier="'+h.publicIdentifier+'" data-profile_pic="'+profile_pic+'" data-seenAt="'+seenAt+'" style="font-weight: "'+h.font_weight+'"><div class="linkedin_thread_profile_pic"><img src="'+profile_pic+'" /></div><div class="linkedin_thread_profile_nmd"><div class="linkedin_thread_name"><div class="thread-name">'+h.name+'</div>  </div>';

                                linkedin_threads_html += "<tr><td class='linkedin_thread_subject' colspan='3'>";

                                if(h.inmails){
                                    linkedin_threads_html += "<div class='thread-subject linkedin_thread_subject_text'><strong>InMail - </strong>"+h.subject
                                }else{
                                    linkedin_threads_html += '<div class="thread-subject linkedin_attach_subject linkedin_thread_subject_text">';

                                    if(h.subject == "Attachment"){
                                        linkedin_threads_html += '<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false"><path d="M5,2h5.5c3,0,5.5,2.5,5.5,5.5S13.5,13,10.5,13H4c-2.2,0-4-1.8-4-4s1.8-4,4-4h5.5C10.9,5,12,6.1,12,7.5S10.9,10,9.5,10H5V8.1h4.5c0.3,0,0.6-0.3,0.6-0.6c0,0,0,0,0,0c0-0.3-0.3-0.6-0.6-0.6c0,0,0,0,0,0H4C2.9,6.8,1.9,7.6,1.9,8.7c0,0.1,0,0.2,0,0.3c-0.1,1.1,0.8,2.1,1.9,2.1c0.1,0,0.2,0,0.2,0h6.6c1.9,0,3.5-1.6,3.5-3.5c0,0,0-0.1,0-0.1c0.1-1.9-1.5-3.6-3.4-3.6c0,0,0,0-0.1,0H5V2z" class="small-icon" style="fill-opacity: 1"></path></svg>';    
                                    }
                                    linkedin_threads_html += h.subject;
                                }

                                linkedin_threads_html += '</div></div><div class="linkedin_thread_date">'+createdAt+'</div></div>';

                                // if(g == 0){
                                //     chrome.runtime.sendMessage({greeting: "linkedin_conversation_request_1", linkedin_conversation_id: h.conversations_id, linkedin_conversation_title: h.name, linkedin_conversation_image: profile_pic, linkedin_conversation_occupation: occupation, linkedin_conversation_seenAt: seenAt, linkedin_conversation_publicIdentifier: h.publicIdentifier}, function(response) {

                                //     });
                                // }

                            });
                            
                            //$('body').find('.linkedin_threads_table').html(linkedin_threads_html);
                            $('body').find('.linkedin_threads_table').append(linkedin_threads_html);

                            window.thread_repeat = true;

                         }

                         if(request.ocean == "linkedin_threads_unread_count_response_2"){
                            console.log("linkedin unread count");
                            console.log(request.linkedin_unread_count);

                            $.each(request.linkedin_unread_count.elements ,function(i, e){
                                if(e.tab == "MESSAGING"){
                                    if(e.count != 0){
                                        $('.linkedin_unread_count').html(e.count);
                                    }
                                }
                            });

                         }

                    });



                $('body').on('click', '.linkedin_mailbox_back_btn', function(){
                    $('body').find('#linkedin_label').click();
                });

                


            //}, 6000);

        }else{
                console.log("check_gmailbody_load on");
            }
        }, 1000);
        
    }

    
    if( window.location.origin == "https://www.linkedin.com") {

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "linkedin_threads_response_1") {
                
                var res = callXHROnLinkedIn("https://www.linkedin.com/voyager/api/messaging/conversations?keyVersion=LEGACY_INBOX", [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");

                    chrome.runtime.sendMessage({greeting: "linkedin_threads_request_2", result:a }, function(response) {});

                });

            }
            if(request.ocean == "linkedin_threads_scroll_response_1"){

                callXHROnLinkedIn("https://www.linkedin.com/voyager/api/messaging/conversations?keyVersion=LEGACY_INBOX&createdBefore="+request.most_recent_thread_timestamp, [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");

                    chrome.runtime.sendMessage({greeting: "linkedin_threads_scroll_request_2", result:a }, function(response) {
                    });

                });

            }

            if(request.ocean == "linkedin_threads_unread_count_response_1"){

                callXHROnLinkedIn("https://www.linkedin.com/voyager/api/voyagerCommunicationsTabBadges?q=tabBadges&countFrom=0", [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    console.log(a);
                    // a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");

                    chrome.runtime.sendMessage({greeting: "linkedin_threads_unread_count_request_2", result:a }, function(response) {
                    });

                });

            }   
        });
        
    }


    

});
