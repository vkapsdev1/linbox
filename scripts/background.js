chrome.runtime.onMessage.addListener(
function(request, sender, sendResponse) {
console.log(sender.tab ?
            "from a content script:" + sender.tab.url :
            "from the extension");

 if( request.greeting == "linkedin_threads_request_1") {

	  chrome.tabs.query({}, function(tabs) {
		    var message = {ocean: "linkedin_threads_response_1"};
		    var once = true;
		    for (var i1=0; i1<tabs.length; i1++) { 
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
		    		once = false;
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}	        
		    }
		    if(once){
		    	for (var i1=0; i1<tabs.length; i1++) { 
		    		var message = {ocean: "linkedin_threads_no_response"};
			    	var tab_url = tabs[i1].url;
			    	if(tab_url.indexOf('mail.google.com') > 0){
			    		chrome.tabs.sendMessage(tabs[i1].id, message);
			    	}        
			    }
		    }
		});
	}

 if( request.greeting == "linkedin_threads_request_2") {
 	console.log('back 2 call');
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_threads_response_2", result: request.result};
	    for (var i2=0; i2<tabs.length; i2++) {
	    	console.log(tabs[i2]);
	        chrome.tabs.sendMessage(tabs[i2].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_conversation_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_conversation_response_1", linkedin_conversation_id: request.linkedin_conversation_id, linkedin_conversation_title: request.linkedin_conversation_title, linkedin_conversation_image: request.linkedin_conversation_image, linkedin_conversation_occupation: request.linkedin_conversation_occupation, linkedin_conversation_seenAt: request.linkedin_conversation_seenAt, linkedin_conversation_publicIdentifier: request.linkedin_conversation_publicIdentifier};
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	    if(once){
	    	for (var i1=0; i1<tabs.length; i1++) { 
	    		var message = {ocean: "linkedin_threads_no_response"};
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('mail.google.com') > 0){
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}        
		    }
	    }
	});
	}

 if( request.greeting == "linkedin_conversation_request_2") {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_conversation_response_2", result: request.result, linkedin_conversation_id: request.linkedin_conversation_id, linkedin_conversation_title: request.linkedin_conversation_title, linkedin_conversation_image: request.linkedin_conversation_image, linkedin_conversation_occupation: request.linkedin_conversation_occupation, linkedin_conversation_seenAt: request.linkedin_conversation_seenAt, linkedin_conversation_publicIdentifier: request.linkedin_conversation_publicIdentifier, owner_linkedin_profile_id: request.owner_linkedin_profile_id};
	    for (var i4=0; i4<tabs.length; i4++) {
	        chrome.tabs.sendMessage(tabs[i4].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_threads_scroll_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_threads_scroll_response_1", most_recent_thread_timestamp: request.thread_timestamp};
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	    if(once){
	    	for (var i1=0; i1<tabs.length; i1++) { 
	    		var message = {ocean: "linkedin_threads_no_response"};
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('mail.google.com') > 0){
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}        
		    }
	    }
	});
	}

 if( request.greeting == "linkedin_threads_scroll_request_2") {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_threads_scroll_response_2", result: request.result};
	    for (var i6=0; i6<tabs.length; i6++) {
	        chrome.tabs.sendMessage(tabs[i6].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_conversation_scroll_request_1") {
 	console.log("ocean1");
	  chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_conversation_scroll_response_1", most_recent_conversation_timestamp: request.conversation_timestamp, linkedin_conversation_id: request.linkedin_conversation_id};
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	    if(once){
	    	for (var i1=0; i1<tabs.length; i1++) { 
	    		var message = {ocean: "linkedin_threads_no_response"};
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('mail.google.com') > 0){
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}        
		    }
	    }
	});
	}

 if( request.greeting == "linkedin_conversation_scroll_request_2" ) {
 	console.log("ocean3");
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_conversation_scroll_response_2", result: request.result, owner_linkedin_profile_id: request.owner_linkedin_profile_id};
	    for (var i8=0; i8<tabs.length; i8++) {
	        chrome.tabs.sendMessage(tabs[i8].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_conversation_sendmsg_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_conversation_sendmsg_response_1", linkedin_conversation_msg_text: request.linkedin_conversation_msg_text, linkedin_conversation_id: request.linkedin_conversation_id, linkedin_conversation_gif_obj: request.linkedin_conversation_gif_obj};
	    var once = true;
	    console.log(message);
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	    if(once){
	    	for (var i1=0; i1<tabs.length; i1++) { 
	    		var message = {ocean: "linkedin_threads_no_response"};
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('mail.google.com') > 0){
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}        
		    }
	    }
	});
 }

 if( request.greeting == "linkedin_conversation_sendmsg_request_2" ) {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_conversation_sendmsg_response_2", result: request.result, linkedin_conversation_msg_text: request.linkedin_conversation_msg_text};
	    console.log(message);
	    for (var i8=0; i8<tabs.length; i8++) {
	        chrome.tabs.sendMessage(tabs[i8].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_gif_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_gif_response_1", search_keyword: request.search_keyword};
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	    if(once){
	    	for (var i1=0; i1<tabs.length; i1++) { 
	    		var message = {ocean: "linkedin_threads_no_response"};
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('mail.google.com') > 0){
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}        
		    }
	    }
	});
 }

 if( request.greeting == "linkedin_gif_request_2" ) {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_gif_response_2", result: request.result};
	    for (var i8=0; i8<tabs.length; i8++) {
	        chrome.tabs.sendMessage(tabs[i8].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_sales_navigator_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_sales_navigator_response_1", linkedin_sales_navigator_email: request.linkedin_sales_navigator_email};
	    console.log(request.linkedin_sales_navigator_email);
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	});
 }

 if( request.greeting == "linkedin_sales_navigator_request_2" ) {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_sales_navigator_response_2", result: request.result, gmail_inbox: request.gmail_inbox};
	    for (var i8=0; i8<tabs.length; i8++) {
	        chrome.tabs.sendMessage(tabs[i8].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_attachment_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	   
	    var message = {ocean: "linkedin_attachment_response_1", attachment_filename: request.attachment_filename, attachment_filesize: request.attachment_filesize, attachment_filetype: request.attachment_filetype,  attachment_fileblob: request.attachment_fileblob};
	    
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	    if(once){
	    	for (var i1=0; i1<tabs.length; i1++) { 
	    		var message = {ocean: "linkedin_threads_no_response"};
		    	var tab_url = tabs[i1].url;
		    	if(tab_url.indexOf('mail.google.com') > 0){
		    		chrome.tabs.sendMessage(tabs[i1].id, message);
		    	}        
		    }
	    }
	});
 }

 if( request.greeting == "linkedin_attachment_request_2" ) {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_attachment_response_2"};
	    for (var i8=0; i8<tabs.length; i8++) {
	        chrome.tabs.sendMessage(tabs[i8].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_threads_unread_count_request_1") {
	  chrome.tabs.query({}, function(tabs) {
	   
	    var message = {ocean: "linkedin_threads_unread_count_response_1"};
	    
	    var once = true;
	    for (var i1=0; i1<tabs.length; i1++) { 
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('linkedin.com') > 0 && once && tab_url.indexOf('/sales/') == -1){
	    		once = false;
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}	        
	    }
	});
 }

 if( request.greeting == "linkedin_threads_unread_count_request_2" ) {
 		chrome.tabs.query({}, function(tabs) {
	    var message = {ocean: "linkedin_threads_unread_count_response_2", linkedin_unread_count: request.result};
	    for (var i8=0; i8<tabs.length; i8++) {
	        chrome.tabs.sendMessage(tabs[i8].id, message);
	    }
	});
 }

 if( request.greeting == "linkedin_threads_no_request" ) {
 		chrome.tabs.query({}, function(tabs) {
	    for (var i1=0; i1<tabs.length; i1++) { 
    		var message = {ocean: "linkedin_threads_no_response"};
	    	var tab_url = tabs[i1].url;
	    	if(tab_url.indexOf('mail.google.com') > 0){
	    		chrome.tabs.sendMessage(tabs[i1].id, message);
	    	}        
	    }
	});
 }



});