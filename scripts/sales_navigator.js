function callXHROnLinkedInConversation(a, c, b, d) {
    $.ajax({
        url: a,
        async: !1,
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "GET",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}

$(document).ready(function(){


    if( window.location.origin == "https://mail.google.com") {

            
        
        var hash = window.location.hash;
        var inbox = (hash.match(/#inbox/g) || []).length;
        var sent = (hash.match(/#sent/g) || []).length;
        var search = (hash.match(/#search/g) || []).length;
        
        if( (inbox > 0 || sent > 0 || search > 0) && (hash.length - inbox > 0 || hash.length - sent > 0 || hash.length - search > 0)){
            var counter =  0;
            var check_inboxbody_load = setInterval(function(){ 

                if($('td.Bu.y3').length > 0){
                    
                    $('td.Bu.y3').prepend("<div class='sales_navigator_box_wrapper' style='position: relative;'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div></div>");

                    clearInterval(check_inboxbody_load);
                    var email = $('span.gD').attr('email');
                    
                    if(typeof email !== "undefined" ){
                        if($('.linkedin_loader_wrap').length <= 0){
                            
                            $('td.Bu.y3').prepend("<div class='sales_navigator_box_wrapper' style='position: relative;'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div></div>");
                            $('.linkedin_loader_wrap').show();
                        }else{
                            $('.linkedin_loader_wrap').show();
                        }
                        chrome.runtime.sendMessage({greeting: "linkedin_sales_navigator_request_1", linkedin_sales_navigator_email: email}, function(response) {});    
                    }
                }
                if(counter == 7){
                    clearInterval(check_inboxbody_load);
                }
                counter++;

            }, 1000);    
        }
        

        $('body').on('click', 'tr.zA', function(){
            console.log("rown clicked");
            var email = $(this).find('span.yP').attr('email');
            
            if(typeof email !== "undefined" ){
                
                if($('td.Bu.y3').length <= 0){
                    setTimeout(function(){ 
                        console.log("rown clicked"+ email); 
                        $('td.Bu.y3').prepend("<div class='sales_navigator_box_wrapper' style='position: relative;'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div></div>");
                    }, 500);       
                    $('.linkedin_loader_wrap').show();
                }else{
                    $('.linkedin_loader_wrap').show();
                }
                
                chrome.runtime.sendMessage({greeting: "linkedin_sales_navigator_request_1", linkedin_sales_navigator_email: email}, function(response) {});    
            }else{
                console.log(email + "email");
                if($('td.Bu.y3').length <= 0){
                    console.log("rown final clicked"+ email);    
                    $('td.Bu.y3').prepend("<div class='sales_navigator_box_wrapper' style='position: relative;'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div></div>");
                    $('.linkedin_loader_wrap').show();
                }else{
                    $('.linkedin_loader_wrap').show();
                }

                email = $(this).find('span.zF').attr('email');
                chrome.runtime.sendMessage({greeting: "linkedin_sales_navigator_request_1", linkedin_sales_navigator_email: email}, function(response) {});    
            }            

        });

        $('body').on('hover', 'span.gD', function(){
            var email = $(this).attr('email');
            
            if(typeof email !== "undefined" ){
                if($('.linkedin_loader_wrap').length <= 0){
                    
                    $('td.Bu.y3').prepend("<div class='sales_navigator_box_wrapper' style='position: relative;'><div class='linkedin_loader_wrap' style='display: none;'><div class='linkedin_loader'><img src='"+chrome.runtime.getURL("images/loader.gif")+"' alt='loader' /></div></div></div>");
                    $('.linkedin_loader_wrap').show();
                }else{
                    $('.linkedin_loader_wrap').show();
                }
                chrome.runtime.sendMessage({greeting: "linkedin_sales_navigator_request_1", linkedin_sales_navigator_email: email}, function(response) {});    
            }
        });


        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "linkedin_sales_navigator_response_2" ) {
                console.log(request);
                if(request.gmail_inbox == 'true'){
                    
                    setTimeout( function(){

                        var sales_navigator_parsed_html = $.parseHTML(request.result);
                        console.log($('.sales_navigator_box_gmail').length + "length");
                        if($('.sales_navigator_box_gmail').length <= 0){
                            $('.sales_navigator_box_wrapper').prepend('<div class="sales_navigator_box sales_navigator_box_gmail"></div>');
                        }else{
                            $('.sales_navigator_box_gmail').html('');  
                        }
                        
                        $.each(sales_navigator_parsed_html, function(i, e){
                            $('.sales_navigator_box_gmail').prepend(e);    
                        });

                        $('.sales_navigator_box_gmail').find('img').remove(".li-img-hdr-snav");
                        $('.sales_navigator_box_gmail').find('div').remove(".li-tabs-profile");
                        $('.sales_navigator_box_gmail').find('button').remove("#li-upgrade");
                        $('.sales_navigator_box_gmail').find('div').remove(".li-footer");
                        $('.sales_navigator_box_gmail').find('div').remove(".li-head");
                        if($('.sales_navigator_box_gmail').find('li').remove(".li-content-teamlink")){
                            $('.sales_navigator_box_gmail').find('.li-tabs-content').css("min-height", "75px")
                        }


                        $('body').find('img.li-icn-panel').each(function(){
                            if($(this).hasClass('li-icon-panel-phn')){
                                $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/phone-icon.png") + ")");    
                            }else if($(this).hasClass('li-icon-panel-li')){
                                $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/linkedin-icon2.png") + ")");    
                            }else if($(this).hasClass('li-icon-panel-twit')){
                                $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/tweet-icon.png") + ")");    
                            }else if($(this).hasClass('li-icon-panel-email')){
                                $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/mail-icon.png") + ")");    
                            }else if($(this).hasClass('li-icon-panel-web')){
                                $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/globe-icon.png") + ")");    
                            }else{
                                $(this).css('display', 'block');    
                            }
                            
                        });

                        $('.linkedin_loader_wrap').hide();

                    }, 2000);
                
                }else{
                    
                    var sales_navigator_parsed_html = $.parseHTML(request.result);

                    if($('.linkedin_mailbox_container_left .sales_navigator_box_linkedin').length <= 0){
                        if(window.initilize){
                            $('.linkedin_threads_table .linkedin_thread_row:first-child').append('<div class="sales_navigator_box sales_navigator_box_linkedin" style="display: none"></div>');
                        }else{
                            $(window.conversation_clicked).append('<div class="sales_navigator_box sales_navigator_box_linkedin" style="display: none"></div>');
                        }
                    }else{
                        $('body').find('.sales_navigator_box').remove();
                        if(request.result != 'no-profile'){
                            $(window.conversation_clicked).append('<div class="sales_navigator_box sales_navigator_box_linkedin" style="display: none"></div>');
                        }
                    }
                    
                    $('.sales_navigator_box_linkedin').prepend('<span class="sales_navigator_close_btn">CLOSE</span>');

                    $.each(sales_navigator_parsed_html, function(i, e){
                        $('.sales_navigator_box_linkedin').prepend(e);    
                    });

                    $('.sales_navigator_box_linkedin').find('img').remove(".li-img-hdr-snav");
                    $('.sales_navigator_box_linkedin').find('div').remove(".li-tabs-profile");
                    $('.sales_navigator_box_linkedin').find('button').remove("#li-upgrade");
                    $('.sales_navigator_box_linkedin').find('div').remove(".li-footer");
                    $('.sales_navigator_box_linkedin').find('div').remove(".li-head");
                    if($('.sales_navigator_box_linkedin').find('li').remove(".li-content-teamlink")){
                        $('.sales_navigator_box_linkedin').find('.li-tabs-content').css("min-height", "75px")
                    }

                    $('body').find('img.li-icn-panel').each(function(){
                        if($(this).hasClass('li-icon-panel-phn')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/phone-icon.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-li')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/linkedin-icon2.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-twit')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/tweet-icon.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-email')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/mail-icon.png") + ")");    
                        }else if($(this).hasClass('li-icon-panel-web')){
                            $(this).closest('li').find('.li-icn-panel-pop').css("background-image", "url(" + chrome.runtime.getURL("images/globe-icon.png") + ")");    
                        }else{
                            $(this).css('display', 'block');    
                        }
                        
                    });

                    $('.sales_navigator_box_linkedin').show('slow');
                    $('.linkedin_loader_wrap').hide();

                    $('body').on('click', '.sales_navigator_close_btn', function(){
                        $('.sales_navigator_box_linkedin').hide('slow');
                    });

                }

            }
        });


    }
    if( window.location.origin == "https://www.linkedin.com") {
        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "linkedin_sales_navigator_response_1" ) {
                callXHROnLinkedInConversation("https://www.linkedin.com/sales/gmail/profile/viewByEmail/"+request.linkedin_sales_navigator_email, [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {
                    
                        chrome.runtime.sendMessage({greeting: "linkedin_sales_navigator_request_2", result:a, gmail_inbox: 'true' }, function(response) {
                        
                    });

                });
            }
        });
    }

});


