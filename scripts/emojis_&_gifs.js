function callXHROnLinkedInAttachments(filename, filesize, filetype, a, c, b, d){

    if(filetype.indexOf("image") >= 0){
        var pdata = {
                  mediaUploadType: 'MESSAGING_PHOTO_ATTACHMENT',
                  fileSize: filesize,
                  filename: filename
                };
    }else{
        var pdata = {
                  mediaUploadType: 'MESSAGING_FILE_ATTACHMENT',
                  fileSize: filesize,
                  filename: filename
                };
    }
    
    $.ajax({
        url: a,
        data: JSON.stringify(pdata),
        async: !1,
        headers: { "Accept":"application/vnd.linkedin.normalized+json+2.1", "Content-Type":"application/json; charset=UTF-8" },
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "POST",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}
function callXHROnLinkedInAttachments_second_call(file, a, c, b, d){

                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function(){
                    if(xhr.readyState == 4 && xhr.status == 201){
                        "function" == typeof b && b(a)
                    } else {
                        "function" == typeof b && b()
                            
                    }
                };
                xhr.open("PUT",a, false);
                xhr.send(file);

}

function callXHROnLinkedInConversationAttachmentCreate(pdata, a, c, b, d) {

    $.ajax({
        url: a,
        data: JSON.stringify(pdata),
        async: !1,
        headers: { "Accept":"application/vnd.linkedin.normalized+json+2.1", "Content-Type":"application/json; charset=UTF-8" },
        beforeSend: function(a) {
            var JSESSIONID_REGEX = new RegExp('JSESSIONID=["]*(.*?)["]*;');
            var b = document.cookie.match(JSESSIONID_REGEX)[1];
            a.setRequestHeader("csrf-token", b);
            c && 0 < c.length && c.forEach(function(b) {
                a.setRequestHeader(b.key, b.val)
            })
        },
        xhrFields: {
            withCredentials: !0
        },
        type: "POST",
        success: function(a) {
            "function" == typeof b && b(a)
        },
        error: function(a) {
            "function" == typeof b && b()
        }
    })
}

$(document).ready(function(){

    if( window.location.origin == "https://mail.google.com") {

        $('body').on('click', '.gif-icon', function(){

            if($('.linkedin_gif_box').length <= 0){
                $(this).parent().append("<div class='linkedin_gif_box'><div class='linkedin_gif_search'><input class='linkedin_gif_search_box' type='text'><button class='linkedin_gif_search_btn'><img src='"+chrome.runtime.getURL("images/search-icon.png")+"' alt='' /></button></div><div class='linkedin_gif_container'></div></div>");
                chrome.runtime.sendMessage({greeting: "linkedin_gif_request_1", search_keyword: ""}, function(response) {});
            }else{
                $('.linkedin_gif_box').show();
            }

        });

        $('body').on('click', '.attach-icon', function(){
            $(this).next('.linkedin_attachment_icon').click();  
        });

        $('body').on('change', '.linkedin_attachment_icon', function(e){
            e.target.baseURI = "https://www.linkedin.com/";

            var ocean_native_file1 = $(this).val();

            console.log($(this).val());

            console.log(e);

            var fileName = e.target.files[0].name;
            var fileSize = e.target.files[0].size;
            var fileType = e.target.files[0].type;
            var file = e.target.files[0];
            var blob_url = URL.createObjectURL(file);

            var byteSize = fileSize;
            if(fileSize > 1024 ){

                byteSize = Math.round(fileSize/1024) + "KB";
                if(byteSize > 1024){
                    byteSize = Math.round(byteSize/1024) + "MB";
                }
            }else{
                byteSize = fileSize + "b";
            }


            var conversation_id = $('.linkedin_conversation_section').data('conversation_id');

            var storage_key = e.target.files[0].size+"_attachment_urn";
            if(fileType == "image/png"){
                $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><img src='"+blob_url+"' /><span class='linkedin_attachment_name'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                
                setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }    
            if(fileType == "image/jpeg"){
                $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><img src='"+blob_url+"' /><span class='linkedin_attachment_name'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }                

            if(fileType == "application/vnd.ms-excel"){
                $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><span class='linkedin_attachment_name xls_attachment'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }

            if(fileType == "application/pdf"){
                $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><span class='linkedin_attachment_name pdf_attachment'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }
            
            if(fileType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
                 $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><span class='linkedin_attachment_name doc_attachment'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                 setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }

            if(fileType == "application/octet-stream" || fileType == "application/zip" || fileType.indexOf("video") >= 0){
                  $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><img class='file_attachment' src='"+chrome.runtime.getURL("images/file-icon.png")+"' /><span class='linkedin_attachment_name'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                  setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }

            if(fileType == "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
                $('.linkedin_attachment_box').append("<div class='linkedin_attachment' data-storage_key='"+storage_key+"'><span class='linkedin_attachment_name ppt_attachment'>"+fileName+"</span><span class='linkedin_attachment_size'>"+byteSize+"</span><div>Attached</div><div class='loader-box active'></div><span class='linkedin_attachment_remove_btn'>X</span></div>");
                setTimeout(function(){
                    $('.linkedin_attachment_box .linkedin_attachment').find('.active').addClass('show');
                    setTimeout(function(){
                        $('.linkedin_attachment_box .linkedin_attachment').find('.active').hide('active show');
                    }, 1500);
                }, 500);
            }
            
            chrome.runtime.sendMessage({greeting: "linkedin_attachment_request_1", attachment_filename: fileName, attachment_filesize: fileSize, attachment_filetype: fileType, attachment_fileblob: blob_url, linkedin_conversation_id: conversation_id}, function(response) {});
        });

        $('body').on('click', '.linkedin_attachment_remove_btn', function(){
            var key = $(this).closest('.linkedin_attachment').data('storage_key');;
            chrome.storage.local.remove([key]);
            $(this).closest('.linkedin_attachment').remove();
        });

        $('body').on('keyup', '.linkedin_gif_search_box', function(){
            if($(this).val().length > 3){
                var search_keyword = $(this).val();
                chrome.runtime.sendMessage({greeting: "linkedin_gif_request_1", search_keyword: search_keyword}, function(response) {});
            }
        });

        $('body').click(function(e){
            if($(e.target).is('.gif-icon, .linkedin_gif_box *'))return;
            $('.linkedin_gif_box').hide();
        });

        $('body').on('click', '.emoji-icon', function(){
            console.log($('.linkedin_emoji_box').length > 0);
            if($('.linkedin_emoji_box').length  == 0){
                console.log($('.linkedin_emoji_box').length);
                $(this).parent().append('<div class="linkedin_emoji_box"></div>');
                $('.linkedin_emoji_box').load(chrome.runtime.getURL("html/emojis.html"));
            }else{
                $('.linkedin_emoji_box').show();
            }
        });

        $('body').on('click', '.emoji-button', function(){
            $('.linkedin-msg-textarea').append($(this).text().trim());
        });

        $('body').click(function(e){
            if($(e.target).is('.emoji-icon, .linkedin_emoji_box *'))return;
            $('.linkedin_emoji_box').hide();
        });

        $('body').on('click', '.gif_li', function(e){

            var gif_id = $(this).data('gif_id');
            console.log(gif_id);
            console.log(window.media);
            if(typeof window.media !== "undefined"){
                $.each(window.media, function(i, e){    
                    if(e.id == gif_id){

                        var conversation_id = $('.linkedin_conversation_section').data('conversation_id');
                        console.log(conversation_id);
                        chrome.runtime.sendMessage({greeting: "linkedin_conversation_sendmsg_request_1", linkedin_conversation_msg_text: "", linkedin_conversation_id: conversation_id, linkedin_conversation_gif_obj: e}, function(response) {

                            console.log(response);
                        });

                        return false;
                    }
                });
            }
        });

        $('body').on('click', '.emoji-category-tab', function(){
            var content_id = $(this).data('content_id');

            console.log(' content_id ' + content_id);
            $('.emoji-category-tab').removeClass("active");
            $(this).addClass("active");

            var count = $('.emoji-popover__emojis-section').length;
            var total_height = 0;
            var got_it_flag = false;
            $('.emoji-popover__emojis-section').each(function(){

                if( content_id != $(this).attr('id') && !got_it_flag ) {
                    total_height = total_height + $(this).outerHeight(true);
                } else {
                    got_it_flag = true;
                }

                if(!--count){
                    console.log(total_height);
                    if( total_height > 0 ) total_height = total_height + (total_height*1.9)/100;
                    $('.emoji-popover__emojis').animate({scrollTop:total_height}, 'fast');
                }

            });
            
        });

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
            if( request.ocean == "linkedin_gif_response_2" ) {
                console.log(request.result);

                window.media = request.result;

                var linkedin_gif_html = '<ul>';

                $.each(request.result, function(i, e){
                    linkedin_gif_html += '<li class="gif_li" data-gif_id="'+e.id+'"><img src="'+e.media.previewgif.url+'" /></li>';
                });
                linkedin_gif_html += '</ul>';

                $('.linkedin_gif_container').html(linkedin_gif_html);
            }

        });
    }
    if( window.location.origin == "https://www.linkedin.com") {
        

        chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

            if( request.ocean == "linkedin_gif_response_1" ) {
                if(request.search_keyword != ''){
                    var url = "https://www.linkedin.com/voyager/api/messaging/thirdPartyMedia?q=gifSearch&query="+request.search_keyword;
                }else{
                    var url = "https://www.linkedin.com/voyager/api/messaging/thirdPartyMedia?q=gifSearch";
                }
                callXHROnLinkedInConversation(url, [{
                    key: "x-restli-protocol-version",
                    val: "2.0.0"
                }], function(a) {

                    a && a.elements && 0 < a.elements.length ? console.log(a.elements) : console.log("Request failed");
                    chrome.runtime.sendMessage({greeting: "linkedin_gif_request_2", result:a.elements }, function(response) {
                    });

                });
            }

            if( request.ocean == "linkedin_attachment_response_1" ){
                window.linkedin_blob_url = '';
                var x = new XMLHttpRequest();
                x.onload = function() {
                    var result = x.response;

                    var binaryData = [];
                    binaryData.push(result);
                    
                    var file = new Blob(binaryData, {type: request.attachment_filetype});

                    var new_u = URL.createObjectURL(new Blob(binaryData, {type: request.attachment_filetype}));
                    
                    var url = "https://www.linkedin.com/voyager/api/voyagerMediaUploadMetadata?action=upload";
                    console.log(request);
                    callXHROnLinkedInAttachments(request.attachment_filename, request.attachment_filesize, request.attachment_filetype, url, [{
                        key: "x-restli-protocol-version",
                        val: "2.0.0"
                    }], function(a) {
                        var url = a.data.value.singleUploadUrl;
                        var attachment_id = a.data.value.urn;

                        var xhr = new XMLHttpRequest();
                        xhr.onreadystatechange = function(){
                            if(xhr.readyState == 4 && xhr.status == 201){
                                console.log("second call");

                                var guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

                                var pdata = {
                                              eventCreate: {
                                                originToken: guid,
                                                value: {
                                                  'com.linkedin.voyager.messaging.create.MessageCreate': {
                                                    attributedBody: {
                                                      text: "",
                                                      attributes: []
                                                    },
                                                    attachments: [{"id":attachment_id,"name":request.attachment_filename,"byteSize":request.attachment_filesize,"mediaType":request.attachment_filetype,"reference":{"string":new_u}}],
                                                    extensionContent: {}
                                                  }
                                                }
                                              },
                                              dedupeByClientGeneratedToken: false
                                            };

                                chrome.storage.local.set({[request.attachment_filesize+'_attachment_urn']: JSON.stringify({"id":attachment_id,"name":request.attachment_filename,"byteSize":request.attachment_filesize,"mediaType":request.attachment_filetype,"reference":{"string":new_u}})}, function() {
                                });
                                
                                // console.log({"id":attachment_id,"name":request.attachment_filename,"byteSize":request.attachment_filesize,"mediaType":request.attachment_filetype,"reference":{"string":new_u}});
                                // callXHROnLinkedInConversationAttachmentCreate(pdata, "https://www.linkedin.com/voyager/api/messaging/conversations/6534800480432939008/events?action=create", [{
                                //     key: "x-restli-protocol-version",
                                //     val: "2.0.0"
                                // }], function(a) {
                                //     console.log("final creation");
                                //     console.log(a);
                                // });

                            } else {
                                console.log(xhr.status);
                            }
                        };
                        xhr.open("PUT",url, false);
                        xhr.send(file);

                    });
                     // TODO: Use [object ArrayBuffer]
                };
                x.open('GET', request.attachment_fileblob); // <-- blob:-url created in content script
                x.responseType = 'arraybuffer';
                x.send();
            }

        });
        
    }


    

});
