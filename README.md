LinkedIn messaging for Gmail

This extension allows you to check your linkedin messages on Gmail window. If you frequently use Gmail and Linkedin at your workplace and have to switch the tabs again and again, anytime you got a new message on linkedin or a new email on Gmail then this extension solves your problem, saves your time and increase your productivity at the same time :)

To install this extension you need to follow these steps.

1. Download it form here.
2. Unzip it.
3. Open you chrome browser and open chrome://extensions
4. Enable Developer option.
5. Load the unpacked extension.


To use this extension you need to follow these steps 

1. Open your Gmail inbox and if already opened then refresh the page once.
2. You will see "Linkedin Inbox" and "Sales Navigator Inbox" option in left side menus.
3. Login into linkedin in a new tab in the same browser and just leave it.
4. When you click on the linkedin inbox menu, it loads all the chats and their conversations on your screen where you can communicate with your connections.